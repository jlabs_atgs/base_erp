<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name',100)->after('name');
            $table->string('middle_name',100)->after('first_name')->nullable();
            $table->string('last_name',100)->after('middle_name');
            $table->string('name_extension',10)->after('last_name')->nullable();
            $table->integer('status')->after('name_extension');
            $table->text('image')->after('password')->nullable();
            $table->text('signature')->after('image')->nullable();
            $table->integer('company_id')->after('signature');
            $table->integer('role_id')->after('company_id');
            $table->integer('position_id')->after('role_id');
            $table->integer('department_id')->after('position_id');
            $table->enum('view_mode',['0','1'])->after('department_id')->comment('0 = All Access, 1 = View Only')->default('0');
            $table->string('view_mode_modules',100)->after('view_mode')->nullable();
            $table->string('registered_ip',100)->after('view_mode_modules');
            $table->string('session_id',255)->after('registered_ip')->nullable();
            $table->integer('created_by')->after('updated_at')->nullable();
            $table->integer('updated_by')->after('created_by')->nullable();
            $table->dateTime('created_at')->change();
            $table->dateTime('updated_at')->change();
        }); 
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('middle_name');
            $table->dropColumn('last_name');
            $table->dropColumn('name_extension');
            $table->dropColumn('status');
            $table->dropColumn('image');
            $table->dropColumn('signature');
            $table->dropColumn('company_id');
            $table->dropColumn('role_id');
            $table->dropColumn('position_id');
            $table->dropColumn('department_id');
            $table->dropColumn('view_mode');
            $table->dropColumn('view_mode_modules');
            $table->dropColumn('registered_ip');
            $table->dropColumn('session_id');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
        });
    }
}
