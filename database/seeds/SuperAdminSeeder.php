<?php

use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check  =	DB::table('users')
				        ->where('id', 1)
					    ->count();
		if ($check == 0) {
			DB::table('users')
		        ->insert([
		        	'id' 		 	=> 1,
		        	'name' 		 	=> 'superadmin',
		        	'first_name' 	=> 'ATGS',
		        	'last_name'  	=> 'Corp',
		        	'status' 	 	=> 1,
		        	'email' 	 	=> 'allan.yumul@atgscorp.com',
		        	'password' 	 	=> bcrypt('123456'),
		        	'company_id' 	=> 0,
		        	'role_id'    	=> 0,
		        	'position_id'	=> 0,
		        	'department_id' => 0,
		        	'view_mode' 	=> '0',
		        	'registered_ip' => '127.0.0.1',
		        	'created_at' 	=> date('Y-m-d')
		        ]);
		}else{
			return false;
		}
    }
}
