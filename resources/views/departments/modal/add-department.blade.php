<style>
    .parsley-error {
      border-color: #ff5d48 !important;
    }
    .parsley-errors-list.filled {
      display: block;
    }
    .parsley-errors-list {
      display: none;
      margin: 0;
      padding: 0;
    }
    .parsley-errors-list > li {
      font-size: 12px;
      list-style: none;
      color: #ff5d48;
      margin-top: 5px;
    }
    .form-section {
      padding-left: 15px;
      border-left: 2px solid #FF851B;
      display: none;
    }
    .form-section.current {
      display: inherit;
    }
    .t {
      background-color:#5ba2e6; 
      color: #fff;
    }
    .card-header{
      background-color: #4980b5;
      color: #fff;
    }
</style>
<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="add-customer-type-modal" aria-hidden="true" id="add-department-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Department</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>              
            </div>
            <div class="modal-body">
                <form id="add-department-form" data-parsley-validate novalidate>
                  {{ csrf_field() }}
                  <div class="row"> 
                      <div class="col-md-8 col-xs-12 col-lg-8 col-sm-8">
                          <div class="form-group">
                              <label>Name <span style="color: red">*</span></label>
                              <input type="text" name="name" class="form-control" required>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12"> 
                         <button type="submit" class="btn btn-primary department-finish"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN Java Script for this page -->
<script src="{{ asset('/plugins/parsleyjs/parsley.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
      //add chart of account
      if ( $('#add-department-form').length > 0 ) {
          $('#add-department-form').submit(function(e){
              var formData = $(this).serialize();
              swal({
                title: "Are you sure?",
                text: "Saving Data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willSave) => {
                if (willSave) {
                  $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
                  })
                  $.ajax({
                     url: '/departments/addDepartment',
                     type: "POST",
                     data: formData,
                     beforeSend: function() {
                        $('.department-finish').attr('disabled', 'disabled');
                     },
                     success: function(response) {
                         if (response != '') {
                            $('#get-department-table').DataTable().destroy();
                            getDepartmentTable();
                            $('#add-department-form').trigger("reset");
                            swal("Department has been saved!", {
                              icon: "success",
                            });
                         }
                     },
                     complete: function() {
                        $('.department-finish').removeAttr('disabled');
                     }
                  });
                } else {
                  swal("Save Aborted");
                }
              });
              e.preventDefault();
              return false;
          })
      }
  })
</script>
<!-- END Java Script for this page -->
<script src="{{ asset('/js/sweetalert.js') }}"></script>
