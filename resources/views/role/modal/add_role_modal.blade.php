<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="add_role_modal" aria-hidden="true" id="add_role_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Access Role</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>              
            </div>
            <div class="modal-body">
                <form id="add-role-information" data-parsley-validate novalidate>
                  {{ csrf_field() }}
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Access Role Name</label>
                              <input class="form-control" name="role_name" type="text" required  />
                          </div>
                      </div>
                  </div>

                  <hr>
                  <div class="row">
                      <div class="col-lg-12">
                        <h6>COMPANY</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 0 OR $value->id == 1 )
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                        <h6>REQUESTS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 38 OR $value->id == 25 OR $value->id == 40 OR $value->id == 49 OR $value->id == 47 OR $value->id == 50)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                        <h6>SALES</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 9 OR $value->id == 10 OR $value->id == 11)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>
                          
                          <h6>RECEIPTS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 12 OR $value->id == 13 )
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                           <h6>PURCHASES</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 3 OR $value->id == 4 OR $value->id == 5)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                          <h6>DISBURSEMENTS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 6 OR $value->id == 7 OR $value->id == 8)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>
                          
                          <h6>INVENTORIES</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 26 OR $value->id == 27 OR $value->id == 42 OR $value->id == 43 OR $value->id == 28)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                        <h6>GENERAL JOURNALS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id == 14)
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                        <h6>REPORTS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id >= 15 && $value->id <= 24 OR $value->id == 34 OR $value->id == 37 )
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>

                        <h6>CONFIGURATIONS</h6>
                        @foreach($permissions as $key => $value)
                          @if ($value->id >= 29 && $value->id <= 36 )
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input permissions" type="checkbox" name="permissions[]" value="{{ $value->id }}"> {{ $value->permission_name }}
                              </label>
                          </div>
                          @endif
                        @endforeach
                          <hr>




                      </div>
                  </div>
                  <br>
                  <div class="row">
                      <div class="col-lg-12"> 
                         <button type="submit" class="btn btn-primary user-finish btn-sm"><i class="fa fa-save" aria-hidden="true"></i></button>
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('user.modal.copy-user-role-modal')
<!-- BEGIN Java Script for this page -->
<script src="{{ asset('/plugins/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap3-typeahead.js') }}"></script>

<script>
  $('#copy-user-role-modal').click(function(e){
     $('#copy-user-role').modal('show');
     return false;
     e.preventDefault();
  })
  $('#form').parsley();

   $(document).ready(function(){
     if( $('#add-role-information').length > 0 ) {
       $('#add-role-information').submit(function(e){
          var formData = $(this).serialize();
          swal({
            title: "Are you sure?",
            text: "Once saved, you will not be able to change your key information!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willSave) => {
            if (willSave) {
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
              })
              $.ajax({
                 url: '/roles/saveRoleInformation',
                 type: "POST",
                 data: formData,
                 beforeSend: function() {
                    $('.role-finish').attr(', ');
                 },
                 success: function(response) {
                     if (response != '') {
                        $('.get-roles-table').html(response);
                        $('#add-role-information').trigger("reset");
                        swal("Role has been saved!", {
                          icon: "success",
                        });
                     }
                 },
                 complete: function() {
                    $('.role-finish').removeAttr('disabled');
                 }
              });
            } else {
              swal("Save Aborted");
            }
          });
          e.preventDefault();
          return false;
       })
     }
   })
</script>
<!-- END Java Script for this page -->
<script src="{{ asset('/js/sweetalert.js') }}"></script>
