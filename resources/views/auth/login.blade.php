@extends('layouts.auth')

@section('content')
<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
    <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
        <form class="login100-form validate-form" action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
            <span class="login100-form-title p-b-49">
                <div><img alt="Logo" height="100" src="{{ asset('images/logo/logo.jpg') }}"/></div>
            </span>

            @if(session('login_error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('login_error') }}
                        </div>
            @endif

             @if ($errors->has('name'))
                    <div style="text-align: center"><span class="invalid-feedback" role="alert" style="color: red; font-size: 12px; font-style: italic">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span></div>
             @endif

             @if ($errors->has('password'))
                    <div><span class="invalid-feedback" role="alert" style="color: blue; font-size: 12px; font-style: italic">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span></div>
                @endif

            <br>
      
            <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is required">
                <span class="label-input100">Username</span>
                <input class="input100 {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" name="name" placeholder="Type your username">
                <span class="focus-input100" data-symbol="&#xf206;"></span>
            </div>

            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <span class="label-input100">Password</span>
                <input class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }} password-type" type="password" name="password" placeholder="Type your password">
                <span class="focus-input100" data-symbol="&#xf190;"></span>
                <i class="far fa-eye-slash show-password" aria-hidden="true" title="show password" style="float: right; bottom: 35px; position: relative; color: gray; font-size: 20px; margin-right: 10px;"></i>
            </div>
            <br>
            <div class="text-right p-t-8 p-b-31">
                <a href="{{ route('password.request') }}">
                    Change / Forgot Password?
                </a>
            </div>
            
            <div class="container-login100-form-btn">
                <div class="wrap-login100-form-btn">
                    <div class="login100-form-bgbtn"></div>
                    <button class="login100-form-btn" type="submit">
                        Login
                    </button>
                </div>
            </div>
            <br/>
            <div class="flex-c-m">
                <a href="http://www.atgscorp.com" target="_blank"> 
                    Powered by: ATGS Corp
            </div>

<!--             <div class="txt1 text-center p-t-54 p-b-20">
                <span>
                    Or Sign Up Using
                </span>
            </div>

            <div class="flex-c-m">
                <a href="#" class="login100-social-item bg1">
                    <i class="fa fa-facebook"></i>
                </a>

                <a href="#" class="login100-social-item bg2">
                    <i class="fa fa-twitter"></i>
                </a>

                <a href="#" class="login100-social-item bg3">
                    <i class="fa fa-google"></i>
                </a>
            </div> -->
              


        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        if ($('.show-password').length > 0) {
            $('.show-password').click(function(e){
                if ($('.password-type').attr('type') == 'password') {
                    $(this).css('color','blue');
                    $(this).removeAttr('class', 'far fa-eye-slash show-password');
                    $(this).attr('class', 'far fa-eye show-password');
                    $(this).removeAttr('title', 'show password');
                    $(this).attr('title', 'hide password');
                    $('.password-type').removeAttr('type');
                    $('.password-type').attr('type', 'text');    
                }else{
                    $(this).css('color','gray');
                    $(this).removeAttr('class', 'far fa-eye show-password');
                    $(this).attr('class', 'far fa-eye-slash show-password');
                    $(this).removeAttr('title', 'hide password');
                    $(this).attr('title', 'show password');
                    $('.password-type').removeAttr('type');
                    $('.password-type').attr('type', 'password');  
                }
                e.preventDefault();
                return false;
            })
        }  

        $('.show-password').hover(function() {
            $(this).css('cursor','pointer');
        });
    })
</script>

@endsection
