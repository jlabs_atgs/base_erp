<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name', 'Laravel') }} - {{ ucwords(Request::segment(1)) }}</title>
        <meta name="description" content="Free Bootstrap 4 Admin Theme | Pike Admin">
        <meta name="author" content="Pike Web Development - https://www.pikephp.com">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/logo/favicon.jpg') }}">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome CSS -->
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/bootstrap-toggle.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Data table CSS -->
        <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/rowReorder.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
        <!-- input mask -->
        <link href="{{ asset('css/inputmask.css') }}" rel="stylesheet">
        <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
        <!-- Include SmartWizard CSS -->
        <!-- BEGIN CSS for this page -->
        <link href="{{ asset('css/accubooks.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
        <!-- END CSS for this page -->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <style type="text/css">
            body{
                --ck-z-default: 100;
                --ck-z-modal: calc( var(--ck-z-default) + 999 );
            }
        </style>
        @if (Auth::user())
            <script type="text/javascript">
            var global = 3590; //3600 seconds = 1 hour

            function noMovement() {
                if (global == 0) {
                    clearInterval(setInterval(function(){noMovement()}, 1000));
                    $.ajax({
                        type: 'GET',
                        url: '/check-session',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $('#session-modal-timer').modal('show');
                            $('#session-modal-timer').on('shown.bs.modal', function(e){
                                var counter = 10;
                                var counter_timer = setInterval(function() {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                        $('.continue').removeAttr('disabled', 'disabled');
                                        $('#count').show();
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        clearInterval(counter_timer);
                                        $('.continue').attr('disabled', 'disabled');
                                        $('#count').hide();
                                    }
                                }, 1000);
                                $('div').on('click', '.continue', function(e){
                                    setInterval(function(){noMovement()}, 1000);          
                                    $('#session-modal-timer').modal('toggle');
                                    e.preventDefault();
                                    return false;
                                });
                                $('div').on('click', '.not-continue', function(e){
                                    setInterval(function(){noMovement()}, 1000);          
                                    $('#session-modal-timer').modal('toggle');
                                    $('#session-modal').modal('show');
                                    e.preventDefault();
                                    return false;
                                });
                                e.preventDefault();
                                return false;
                            });
                        },
                        error: function (response) {
                            $('#session-modal-timer').modal('toggle');
                            $('#session-modal').modal('show');
                        }
                    });  
                } else {
                    global--;
                }
            }

            function resetGlobal() {
                global=3600;                    
            }    
            $(document).ready(function(){
                $('html').mousemove(function(event){
                    resetGlobal();
                });
                $('html').keypress(function(event){
                    resetGlobal();
                });
            });
            setInterval(function(){noMovement()}, 1000);
            </script>
        @endif
        <script src="{{ asset('js/jquery.steps.min.js') }}"></script>
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-toggle.js') }}"></script>
    </head>
    <body class="adminbody">
        <div id="main">
            <!-- top bar navigation -->
            <div class="headerbar">
                <!-- LOGO -->
                <div class="headerbar-left">
                    <a href="{{ url('/') }}" class="logo"><img alt="Logo" src="{{ asset('images/logo/logo.jpg') }}"/></a>
                </div>
                <div class="headerbar-right pull-right">
                    @if(companyLogo() == '')
                    <a href="{{ url('/') }}" class="logo"><img alt="Logo" src="{{ asset('images/logo/logo.jpg') }}"/></a>
                    @else
                    <a href="{{ url('/') }}" class="logo"><img alt="Logo" src="{{ asset('attachments/'.\Auth::user()->company_id.'/C-CP/'.companyLogo()) }}" /></a>
                    @endif
                </div>
                <nav class="navbar-custom">
                    <ul class="list-inline float-right mb-0">
                        @if(in_array(userRolesPermission(1), userPermissions(), TRUE))
                        <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" title="Company Settings" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="fa fa-cog" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Company Settings</small> </h5>
                                </div>
                                <!-- Company Profile -->
                                <a href="{{ url('company-profile') }}" class="dropdown-item notify-item">
                                <i class="fa fa-building"></i> <span>Company Profile</span>
                                </a>
                                <!-- Departments -->
                                <a href="{{ url('departments/') }}" class="dropdown-item notify-item">
                                <i class="fa fa-sitemap"></i> <span>Departments</span>
                                </a>
                                <!-- Positions -->
                                <a href="{{ url('positions/') }}" class="dropdown-item notify-item">
                                <i class="fa fa-id-badge"></i> <span>Positions</span>
                                </a>
                                <!-- Roles -->
                                <a href="{{ url('roles/') }}" class="dropdown-item notify-item">
                                <i class="fa fa-address-book-o"></i> <span>Roles</span>
                                </a>
                                <!-- Users -->
                                <a href="{{ url('users/') }}" class="dropdown-item notify-item">
                                <i class="fa fa-users"></i> <span>Users</span>
                                </a>
                                <!-- Approvals  -->
                                <a href="{{ url('approval') }}" class="dropdown-item notify-item">
                                <i class="fa fa-thumbs-up"></i> <span>Approval</span>
                                </a>
                                <!-- API Settings  -->
                                <a href="{{ url('api-settings') }}" class="dropdown-item notify-item">
                                <i class="fa fa-plug"></i> <span>API Settings</span>
                                </a>
                                <a href="#" class="dropdown-item notify-item notify-all">&nbsp;
                                </a>
                            </div>
                        </li>
                        @endif
                        <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" title="User Settings" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            @if(profilePicture() == '')
                            <img src="{{ asset('images/avatars/admin.png') }}" alt="Profile image" class="avatar-rounded">
                            @else
                            <img src="{{ asset('attachments/'.\Auth::user()->company_id.'/C-CP/'.profilePicture()) }}" alt="Profile image" class="avatar-rounded">
                            @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                @guest
                                <a href="{{ route('login') }}" class="dropdown-item notify-item">Login</a>
                                <a href="{{ route('register') }}" class="dropdown-item notify-item">Register</a>
                                @else
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>{{ Auth::user()->name }}</small> </h5>
                                </div>
                                <!-- item-->
                                <a href="{{ url('my-profile/') }}" class="dropdown-item notify-item">
                                <i class="fa fa-user"></i> <span>My Profile</span>
                                </a>
                                <!-- item-->
                                <a href="{{ route('logout') }}" class="dropdown-item notify-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-power-off"></i> <span>Logout</span>
                                </a>
                                <a href="#" class="dropdown-item notify-item notify-all">&nbsp;
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                @endguest
                            </div>
                        </li>
                    </ul>
                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
                            <i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- End Navigation -->
            <!-- Left Sidebar -->
            <div class="left main-sidebar">
                <div class="sidebar-inner leftscroll">
                    <div id="sidebar-menu">
                        <ul>
                            <li class="submenu">
                                <a href="{{ url('/') }}"><i class="fa fa-tachometer" ></i><span> DASHBOARD </span> </a>
                            </li>
                        </ul>
                        @if(Auth::user()->role_id == 0 && Auth::user()->company_id == 0)
                        <ul>
                            <li class="submenu">
                                <a href="{{ url('/admin/customer-management') }}"><i class="fa fa-fw fa-bars"></i><span> Customer Management </span> </a>
                            </li>
                            <li class="submenu">
                                <a href="{{ url('/admin/release-notes') }}"><i class="fa fa-fw fa-cogs"></i><span> Release Notes Configuration </span> </a>
                            </li>
                        </ul>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- End Sidebar -->
            <div id="app" class="content-page">
                <main-component></main-component>
            </div>
            <!-- END content-page -->
            <footer class="footer">
                <span class="text-right">
                Copyright <a target="_blank" href="#">Accubooks v2.0.0</a>
                </span>
                <span class="float-right">
                Powered by <a target="_blank" href="http://atgscorp.com/"><b>ATGS Corp</b></a>
                </span>
            </footer>
        </div>

        <!-- END main -->
        <script src="{{ asset('js/app.js') }}"></script>        
        <script src="{{ asset('js/moment.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/select2.min.js') }}"></script>
        <script src="{{ asset('js/global-ajax.js') }}"></script>
        <!-- date time picker js -->
        <script src="{{ asset('js/daterangepicker.js') }}"></script>
        <!-- BEGIN Java Script for this page -->
        <script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}"></script>
        <!-- END Java Script for this page -->
        <script src="{{ asset('js/sweetalert.js') }}"></script>
        <!-- <script src="{{ asset('js/materialize.min.js') }}"></script> -->
        <script src="{{ asset('js/detect.js') }}"></script>
        <script src="{{ asset('js/fastclick.js') }}"></script>
        <script src="{{ asset('js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('js/datatables.js') }}"></script>
        <script src="{{ asset('js/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('js/dataTables.scroller.min.js') }}"></script>
        <script src="{{ asset('js/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script> 

        <!-- App js -->
        <script src="{{ asset('js/pikeadmin.js') }}"></script>
        <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
        <!-- <script src="{{ asset('js/jquery.canvasjs.min.js') }}"></script> -->
        <!-- BEGIN Java Script for this page -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <!-- Counter-Up-->
        <script src="{{ asset('plugins/waypoints/lib/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('plugins/counterup/jquery.counterup.min.js') }}"></script>   
    </body>
</html>