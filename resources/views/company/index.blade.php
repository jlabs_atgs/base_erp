@extends('layouts.app')


@section('content') 
<link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ strtoupper(str_replace("-", " ", Request::segment(1))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-building"></i> Company Details</h3>
                    </div>
                    <div class="card-body">
                        <form id="update-company-profile-form" method="post" data-parsley-validate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="id" value="{{$company_details->id}}" readonly>
                            <div class="row" id="personal-info">
                                <div class="col-lg-9 col-xl-9">
                                    <div class="row">
                                        <!-- First Name -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <span id="company_name_label"></span>
                                                <label class="company_name_label">Company Name</label>
                                                <span id="company_name"></span>
                                                <p class="company_name">{{$company_details->company_name}}</p>
                                            </div>
                                        </div>
                                        <!-- Middle Name -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <span id="company_email_label"></span>
                                                <label class="company_email_label">Email Address</label>
                                                <span id="company_email"></span>
                                                <p class="company_email">{{$company_details->company_email}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- Last Name -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contact Person</label>
                                                <span id="company_contact_person"></span>
                                                <p class="company_contact_person">{{$company_details->contact_person}} </p>
                                            </div>
                                        </div>
                                        <!-- Name Extension -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <span id="company_contact_number"></span>
                                                <p class="company_contact_number">{{$company_details->contact_number}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Fax</label>
                                                <span id="company_fax"></span>
                                                <p class="company_fax">{{$company_details->fax_number}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <span id="company_tin_number_label"></span>
                                                <label class="company_tin_number_label">Tin Number</label>
                                                <span id="company_tin_number"></span> 
                                                <p class="company_tin_number">{{$company_details->tin_number}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Accounting Period</label>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Start</label>
                                                        <span id="accounting_period_start"></span>
                                                        <p class="accounting_period_start">{{$company_details->accounting_period_start}}</p>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>End</label>
                                                        <span id="accounting_period_end"></span>
                                                        <p class="accounting_period_end">{{$company_details->accounting_period_end}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <span id="company_remarks"></span>
                                                <p class="company_remarks">{{$company_details->remarks}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h5>Shipping Address</h5>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Street</label>
                                                <span id="shipping_street"></span>
                                                <p class="shipping_street">{{$company_details->street}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>City</label>
                                                <span id="shipping_city"></span>
                                                <p class="shipping_city">{{$company_details->city}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Province</label>
                                                <span id="shipping_province"></span>
                                                <p class="shipping_province">{{$company_details->region}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <span id="shipping_country"></span>
                                                <p class="shipping_country">{{$company_details->country}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <span id="shipping_zip_code"></span>
                                                <p class="shipping_zip_code">{{$company_details->zip_code}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h5>Billing Address</h5>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Street</label>
                                                <span id="billing_street"></span>
                                                <p class="billing_street">{{$company_details->billing_street}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>City</label>
                                                <span id="billing_city"></span>
                                                <p class="billing_city">{{$company_details->billing_city}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Province</label>
                                                <span id="billing_province"></span>
                                                <p class="billing_province">{{$company_details->billing_region}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <span id="billing_country"></span>
                                                <p class="billing_country">{{$company_details->billing_country}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <span id="billing_zip_code"></span>
                                                <p class="billing_zip_code">{{$company_details->billing_zip_code}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="button" class="btn btn-primary btn-sm" id="edit-company-profile-button" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            <button type="submit" class="btn btn-primary btn-sm" id="update-company-profile-button" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                        </form>
                        <hr>
                        </div>
                        <div class="col-lg-3 col-xl-3 border-left">
                            <form id="upload-profile" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h5>Company Logo</h5> 
                                    <br>
                                    @if(companyLogo() == '')
                                    <img width="50%" src="{{ asset('images/profile/placeholdercompany.jpg')}}">
                                    <br>
                                    <br>
                                    @else
                                    <img src="{{ asset('attachments/'.\Auth::user()->company_id.'/C-CP/'.companyLogo())}}">
                                    <br>
                                    <a href="/company-profile/deleteCompanyLogo/{{\Auth::user()->company_id}}" title="Remove Logo"><i class="fa fa-trash-o fa-fw"></i></a>
                                    <br>
                                    <br>
                                    @endif
                                    <input type="file" name="image" accept=".jpg,.jpeg,.png" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                <hr>
                                <b>Company ID</b>:
                                <br />{{Auth::user()->company_id}}
                                <br />
                                <br />
                                <b>Latest activity</b>:
                                <br />{{date('Y-m-d H:i',strtotime($company_details->updated_at))}} 
                                <br />
                                <br />
                                <b>Register date: </b>: 
                                <br/>{{date('Y-m-d H:i',strtotime($company_details->created_at))}}
                                <br />
                                <br />
                                <b>Register IP: </b>:
                                <br/> {{$company_details->registered_ip}}
                            </form>
                            <hr>
                            <form id="closing-of-books-form">
                                
                                <div class="form-group">
                                    <label><b>Closing of Books(-days)</b></label>
                                    <input type="number" name="closing_of_books" class="form-control" value="{{($company_details->closing_of_book) ? $company_details->closing_of_book : ''}}" min="0" step="1" max="999">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-sm" title="Update" id="closing-of-books-button"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                    
                                </div>
                            </form>
                        </div>
                        </div>                              
                    </div>
                    <!-- end card-body -->                              
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('user.modal.add_user_modal')
<script src="{{ asset('/js/moment.min.js') }}"></script>
<script src="{{ asset('/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/js/sweetalert.js') }}"></script>
<script src="{{ asset('/plugins/parsleyjs/parsley.min.js') }}"></script>
<script type="text/javascript">
    $('#colorpickerfield').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
            $(el).val(hex);
         $('#example').css('backgroundColor','#'+hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        }
    })
    .bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
    });
    if( $('#edit-company-profile-button').length > 0 ) {
        $('#edit-company-profile-button').click(function(e){
            Date.prototype.toInputFormat = function() {
               var yyyy = this.getFullYear().toString();
               var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
               var dd  = this.getDate().toString();
               return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
            };
            var company_name                    = $('.company_name').text();
            var company_email                   = $('.company_email').text();
            var company_contact_person          = $('.company_contact_person').text();
            var company_contact_number          = $('.company_contact_number').text();
            var company_tin_number              = $('.company_tin_number').text();
            var company_fax                     = $('.company_fax').text();
            var remarks                         = $('.company_remarks').text();
            var accounting_period_start         = new Date($('.accounting_period_start').text());
            if(accounting_period_start != "Invalid Date")
            {
                var new_accounting_period_start     = accounting_period_start.toInputFormat();            
            }
            else
            {
                var new_accounting_period_start = "01/01/"+ new Date().getFullYear();
            }
            var accounting_period_end           = new Date($('.accounting_period_end').text());
            if(accounting_period_end != "Invalid Date")
            {
                var new_accounting_period_end     = accounting_period_end.toInputFormat();            
            }
            else
            {
                var new_accounting_period_end       = "12/31/"+ new Date().getFullYear();
    
            }        
            var shipping_street                 = $('.shipping_street').text();
            var shipping_city                   = $('.shipping_city').text();
            var shipping_province               = $('.shipping_province').text();
            var shipping_country                = $('.shipping_country').text();
            var shipping_zip_code               = $('.shipping_zip_code').text(); 
    
            var billing_street                 = $('.billing_street').text();
            var billing_city                   = $('.billing_city').text();
            var billing_province               = $('.billing_province').text();
            var billing_country                = $('.billing_country').text();
            var billing_zip_code               = $('.billing_zip_code').text();
    
    
            // All Append
            // Company Name Label
            $('#company_name_label').append('<label>Company Name <span style="color:red">*</span></label>');
            // Company Email Label 
            $('#company_email_label').append('<label>Email <span style="color:red">*</span></label>');
            // Company Tin Number Label
    
            $('#company_tin_number_label').append('<label>Tin Number <span style="color:red">*</span></label>');
            // Company Name Input - Text
            $('#company_name').append('<input type="text" class="form-control" name="name" id="name" value="'+company_name+'" required/>');
            // Company Email Input - Text
            $('#company_email').append('<input type="text" class="form-control" name="email" id="email" value="'+company_email+'" required/>');
            // Company Contact Person Input - Text
            $('#company_contact_person').append('<input type="text" class="form-control" name="contact_person" id="contact_person" value="'+company_contact_person+'"/>');
            // Company Contact Number Input - Text
            $('#company_contact_number').append('<input type="text" class="form-control contact_number" name="contact_number" id="tin_number" value="'+company_contact_number+'"/>');
            // Company Tin Number Input - Text
            $('#company_tin_number').append('<input type="text" class="form-control tin-mask" name="tin_number" id="contact_number" value="'+company_tin_number+'" required/>');
            // Company Fax
            $('#company_fax').append('<input type="text" class="form-control" name="fax" id="fax" value="'+company_fax+'"/>');
            // Company Account Period Start
            $('#accounting_period_start').append('<input type="text" name="accounting_period_start" class="form-control dates" required="required" value="'+new_accounting_period_start+'">');
            // Company Account Period End
            $('#accounting_period_end').append('<input type="text" name="accounting_period_end" class="form-control dates" required="required" value="'+new_accounting_period_end+'">');
            // Remarks
            $('#company_remarks').append(''     
                +'<textarea class="form-control" name="remarks" rows="4">'+remarks+'</textarea>'
            );
            $('#shipping_street').append('<input type="text" class="form-control" name="shipping_street" id="shipping_street" value="'+shipping_street+'"/>');
            // Shipping City Input - Text
            $('#shipping_city').append('<input type="text" class="form-control" name="shipping_city" id="shipping_city" value="'+shipping_city+'"/>');
            // Shipping Province Input - Text
            $('#shipping_province').append('<input type="text" class="form-control" name="shipping_province" id="shipping_province" value="'+shipping_province+'"/>');
            // Shipping Country Input - Text
            $('#shipping_country').append('<input type="text" class="form-control" name="shipping_country" id="shipping_country" value="'+shipping_country+'"/>');
            // Shipping Zip Code Input - Text
            $('#shipping_zip_code').append('<input type="text" class="form-control" name="shipping_zip_code" id="shipping_zip_code" value="'+shipping_zip_code+'"/>');
            // Billing Street Input - Text
            $('#billing_street').append('<input type="text" class="form-control" name="billing_street" id="billing_street" value="'+billing_street+'"/>');
            // Billing City Input - Text
            $('#billing_city').append('<input type="text" class="form-control" name="billing_city" id="billing_city" value="'+billing_city+'"/>');
            // Billing Province
            $('#billing_province').append('<input type="text" class="form-control" name="billing_province" id="billing_province" value="'+billing_province+'"/>');
            // Billing Country Input - Text
            $('#billing_country').append('<input type="text" class="form-control" name="billing_country" id="billing_country" value="'+billing_country+'"/>');
            // Billing Zip Code Input - Text
            $('#billing_zip_code').append('<input type="text" class="form-control" name="billing_zip_code" id="billing_zip_code" value="'+billing_zip_code+'"/>');
    
            $('.contact_number').inputmask("(9999)-9999-999");
            $('.tin-mask').inputmask("999-999-999-999");
    
            // Company Name Label
            $('.company_name_label').hide();
            // Company Email Label
            $('.company_email_label').hide();
            // Company Tin Number Label
            $('.company_tin_number_label').hide();
            // Company Name
            $('.company_name').hide();
            // Company Email
            $('.company_email').hide();
            // Company Contact Person
            $('.company_contact_person').hide();
            // Company Contact Number
            $('.company_contact_number').hide();
            // Company Tin Number
            $('.company_tin_number').hide();
            // Company Fax
            $('.company_fax').hide();
            // Remarks
            $('.company_remarks').hide();
            // Company Accounting Period Month
            $('.accounting_period_start').hide();
            // Company Accounting Period Month
            $('.accounting_period_end').hide();
            // Shipping Street
            $('.shipping_street').hide();
            // Shipping City
            $('.shipping_city').hide();
            // Shipping Province
            $('.shipping_province').hide();
            // Shipping Country
            $('.shipping_country').hide();
            // Shipping Zip Code
            $('.shipping_zip_code').hide();
            // Billing Street
            $('.billing_street').hide();
            // Billing City
            $('.billing_city').hide();
            // Billing Province
            $('.billing_province').hide();
            // Billing Country
            $('.billing_country').hide();
            // Billing Zip Code
            $('.billing_zip_code').hide();
    
            $('.dates').daterangepicker({
                singleDatePicker: true,
                // minDate : new Date(), maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10)),
                showDropdowns: true
            });
            $(this).hide();
            $('#update-company-profile-button').show();
            e.preventDefault();
            return false;
        })
    }
    $(document).ready(function(){
        $('.dates').daterangepicker({
            singleDatePicker: true,
            // minDate : new Date(), maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10)),
            showDropdowns: true
        });
         $('#update-company-profile-form').submit(function(e){
          var formData = $(this).serialize();
          swal({
            title: "Are you sure?",
            text: "Once saved, you will not be able to change your key information!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willSave) => {
            if (willSave) {
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
              })
              $.ajax({
                 url: '/company-profile/updateCompanyProfile',
                 type: "POST",
                 data: formData,
                 // If success
                 success: function(response) {
                  
                     if (response != '') {
                       
                        swal("Update has been saved!", {
                          icon: "success",
                        });
                        // Get the latest value
                        window.location.reload();
                        $('span').html('');
    
                        // Update Button will hide.
                        $('#update-company-profile-button').hide();
                        // Edit Button will show.
                        $('#edit-company-profile-button').show();
    
    
                     }
                 },
                
              });
            } else {
              swal("Save Aborted");
            }
          });
          e.preventDefault();
          return false;
       })
       $('#upload-profile').submit(function(e){
            var formData = new FormData($(this)[0]);
             swal({
                    title: "Are you sure?",
                    text: "Transaction will be saved to the database.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willSave) => {
                    if (willSave) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        })
                        $.ajax({
                            url: '/company-profile/uploadCompanyLogo',
                            type: "POST",
                            data: formData,
                            cache: false,
                            processData: false, // tell jQuery not to process the data
                            contentType: false, // tell jQuery not to set contentType
                            success: function(response) {
                                if (response === 1) {
                                    swal("Logo has been saved!", {
                                        icon: "success",
                                    });
                                    setTimeout(
                                        function() 
                                        {
                                            window.location.reload();
                                        }, 1500);
                                }
                                else
                                {
                                 swal("Only an IMAGE file is allowed!", {
                                        icon: "error",
                                    });   
                                }
                            },
                        });
                    } else {
                        swal("Update Aborted");
                    }
                });
            e.preventDefault();
            return false;
    
       })
          $('#closing-of-books-form').submit(function(e){
           var formData = $(this).serialize();
           swal({
             title: "Are you sure?",
             text: "Saved!",
             icon: "warning",
             buttons: true,
             dangerMode: true,
           })
           .then((willSave) => {
             if (willSave) {
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
               })
               $.ajax({
                  url: '/company-profile/closingOfBooks',
                  type: "POST",
                  data: formData,
                  // If success
                  success: function(response) {
                      if (response != '') {
                        
                         swal("Update has been saved!", {
                           icon: "success",
                         });
                         // Get the latest value
                         window.location.reload();
                         
                      }
                  },
                 
               });
             } else {
               swal("Save Aborted");
             }
           });
           e.preventDefault();
           return false;
        })
    })
</script>
@endsection