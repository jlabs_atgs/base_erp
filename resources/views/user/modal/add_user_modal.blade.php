<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="add_user_modal" aria-hidden="true" id="add_user_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add new User</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>              
            </div>
            <div class="modal-body">
                <form id="add-user-information" data-parsley-validate novalidate>
                  {{ csrf_field() }}
                  <h3>User Information</h3>
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>First name <span><font color="red">*</font></span></label>
                              <input class="form-control" name="first_name" type="text" id="first-name" required/>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Middle name</label>
                              <input class="form-control" name="middle_name" type="text"/>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Last name <span><font color="red">*</font></span></label>
                              <input class="form-control" name="last_name" type="text" id="last-name" required />
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Name Extension</label>
                              <input class="form-control" name="name_extension" type="text"/>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Email <span><font color="red">*</font></span></label>
                              <input class="form-control" name="email" type="email" id="email" required />
                              <span class="email"></span>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Username <span><font color="red">*</font></span></label>
                              <input class="form-control" name="name" type="text" id="username" data-parsley-username="username" required />
                              <span class="username"></span>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Password <span><font color="red">*</font></span></label>
                              <input class="form-control" name="password" type="text" id="password" data-parsley-length="[6, 12]" required />
                              <span class="password"></span>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Role <span><font color="red">*</font></span> <a href="#" id="copy-user-role-modal" style="font-size: 11px;">copy other users role?</a></label>
                              <input class="form-control" type="text" id="role_name" readonly="readonly" value="Unassigned" required />
                              <input class="form-control" name="role_id" type="hidden" id="role_id" readonly="readonly" value="0" required />
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Position <span><font color="red">*</font></span></label>
                              <input class="form-control typeahead-position" name="position" type="text" id="position" value="Unassigned" required />
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="form-group">
                              <label>Department <span><font color="red">*</font></span></label>
                              <input class="form-control typeahead-department" name="department" type="text" id="department" value="Unassigned"  required />
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12"> 
                         <button type="submit" class="btn btn-primary user-finish"><i class="fa fa-plus" aria-hidden="true"></i></button>
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('user.modal.copy-user-role-modal')
<!-- BEGIN Java Script for this page -->
<script src="{{ asset('/plugins/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap3-typeahead.js') }}"></script>

<script>
  $('#copy-user-role-modal').click(function(e){
     $('#copy-user-role').modal('show');
     return false;
     e.preventDefault();
  })
  $('#form').parsley();
  $('#username').keyup(function(e){
    var username = $(this).val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    e.preventDefault();
    $.ajax({
        url: '/users/checkUsername',
        type: "POST",
        data: {
                username:username   
            },
        success: function(response) {
            $('.username').html('');
            if (response >= 1) {
               $('.username').append('<p style="font-size: 12px; color: #ff5d48; margin-top: 5px;">Username already exists.</p>');
               $('.user-finish').attr('disabled', 'disabled');
            }else{
               $('.user-finish').removeAttr('disabled');
            }
        }
    });

    e.preventDefault();
    return false;
  });
  function positionTypeAhead()
   {
       if( $('.typeahead-position').length > 0 ) {
           $.get('/users/getPosition', function(a) {
               $(".typeahead-position").typeahead({
                   items: "all",
                   autoSelect: false,
                   source: a
               });
           }, "json");
       }
   }
   
   function departmentTypeAhead()
   {
       if( $('.typeahead-department').length > 0 ) {
           $.get('/users/getDepartment', function(a) {
               $(".typeahead-department").typeahead({
                   items: "all",
                   autoSelect: false,
                   source: a
               });
           }, "json");
       }
   }
   $(document).ready(function(){
    setTimeout(
      function() 
      {
         positionTypeAhead();
         departmentTypeAhead();
      }, 2000);
      if( $('#add-user-information').length > 0 ) {
       $('#add-user-information').submit(function(e){
          var formData = $(this).serialize();
          swal({
            title: "Are you sure?",
            text: "Once saved, you will not be able to change your key information!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willSave) => {
            if (willSave) {
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
              })
              $.ajax({
                 url: '/users/saveUserInformation',
                 type: "POST",
                 data: formData,
                 beforeSend: function() {
                    $('.user-finish').attr('disabled', 'disabled');
                 },
                 success: function(response) {
                     if (response != '') {
                        // $('.get-users-table').html(response);
                        $('#add-user-information').trigger("reset");
                        swal("User has been saved!", {
                          icon: "success",
                        });
                                      setTimeout(
                                       function() 
                                       {
                                           window.location.reload();
                                       }, 1000);
                     }
                 },
                 complete: function() {
                    $('.user-finish').removeAttr('disabled');
                 }
              });
            } else {
              swal("Save Aborted");
            }
          });
          e.preventDefault();
          return false;
       })
     }
   })
</script>
<!-- END Java Script for this page -->
<script src="{{ asset('/js/sweetalert.js') }}"></script>
