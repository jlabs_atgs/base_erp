@extends('layouts.app')
@section('content') 
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ ucwords(str_replace("-", " ", Request::segment(1))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-users"></i> Users List ({{$actual_usercount}} / {{$assigned_usercount->user_count}}) 
                          <span class="pull-right">
                            @if ($actual_usercount>=$assigned_usercount->user_count)
                              <button class="btn btn-primary btn-sm" data-toggle="modal" disabled><i class="fa fa-plus" aria-hidden="true" title="Please contact your admin to add more users"></i></button>
                            @else
                              <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add_user_modal"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            @endif
                         </span>
                      </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-users-table">
                            <table id="users-table" class="table table-striped table-bordered table-sm">
                                <thead class="thead-global">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Department</th>
                                        <th>Position</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('user.modal.add_user_modal')
@include('user.modal.view_user_details_modal')
<script type="text/javascript">
    //function in calling datatable ajax
    function getUsersTable(){
      var table = $('#users-table').DataTable({
               ajax: {
                   url: '/users/getAllUsers',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'name' },
                    { data: 'username' },
                    { data: 'email' },
                    { data: 'department' },
                    { data: 'position' },
                    { data: 'role' },
                    { data: 'status' },
                    { data: 'actions' }
               ]
           });
    }
    
    $(document).ready(function(){
        //datatable ajax
        setTimeout(
          function() 
          {
            getUsersTable();
            //search each column datatable
            $('#users-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#users-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
          }, 1000);
        

        //view user modal
        $('div').on('click', '.view-user-details', function(e){
            $('#view-user-details').modal('show');
            $('.user-details').html('');
            var id = this.id;
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            })
            $.ajax({
               url: '/users/getUserDetails',
               type: "POST",
               dataType: "HTML",
               data: {id:id},
               beforeSend: function() {
                    $('.loader').show();
               },
               success: function(response) {
                   if (response != '') {
                        $('.user-details').html(response);
                   }
               },
               complete: function() {
                    $('.loader').hide();
               }
            });
            e.preventDefault();
            return false;
        });
    })
    
</script>
@endsection