<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-header">
                <h3><i class="fa fa-user"></i> User Profile Details</h3>
            </div>
            <div class="card-body">
                <form id="edit-profile-form">
                    <div class="row">
                        <div class="col-lg-9 col-xl-9">
                            <div class="row">
                                <input type="hidden" name="user_id" value="{{ $user_details->id }}" readonly="">
                                <input type="hidden" name="company_id" value="{{ $user_details->company_id }}" readonly="">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>First Name <span><font color="red">*</font></span></label>
                                        <span id="first_name_view"></span>
                                        <p class="first_name_view">{{ $user_details->first_name }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Middle Name <span> </label>
                                        <span id="middle_name_view"></span>
                                        <p class="middle_name_view">{{ $user_details->middle_name }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Last Name  <span><font color="red">*</font></span></label>
                                        <span id="last_name_view"></span>
                                        <p class="last_name_view">{{ $user_details->last_name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Name Extension </label>
                                        <span id="name_extension_view"></span>
                                        <p class="name_extension_view">{{ $user_details->name_extension }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Username <span><font color="red">*</font></span></label>
                                        <span id="username_view"></span>
                                        <p class="username_view">{{ $user_details->username }}</p>
                                        <span class="username"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email <span><font color="red">*</font></span> </label>
                                        <span id="email_view"></span>
                                        <p class="email_view">{{ $user_details->email }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>View Mode <span><font color="red">*</font></span> </label>
                                        <span id="view_mode_view"></span>
                                        <p class="view_mode_view">{{ $user_details->view_mode=='0' ? 'OFF' : 'ON' }}</p>
                                    </div>
                                    <div class="form-group">
                                         <label>View Mode Only:</label>
                                         @if($user_details->view_mode != '0')
                                             @if($view_mode)
                                                 @foreach($view_mode as $modules)
                                                 <p class="view_mode_name_view">{{ $modules->role_permission->permission_name }}</p>
                                                 @endforeach
                                             @endif
                                         @endif
                                             <?php $counter = 0;?>
                                             <span id="module_view" style="display: none;">
                                                 <select class="form-control allowed_acces" name="allowed_access[]" multiple="multiple">
                                                     <option></option>
                                                     @foreach ($all_role_permission as $role_list)
                                                             @if(!empty($role_list->role_permission))
                                                                 @if(in_array($role_list->id,$module_id,TRUE))
                                                                     <option value="{{ $role_list->id }}" selected="selected">{{ $role_list->role_permission->permission_name }}</option>
                                                                 @else
                                                                     <option value="{{ $role_list->id }}">{{ $role_list->role_permission->permission_name }}</option>
                                                                 @endif
                                                             @endif
                                                     @endforeach
                                                 </select>
                                             </span> 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <span style="font-size: 12px; color: red; display: none;" class="notification"><i>Department has been changed. All the approval method of this user's previous department will be deleted.</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-primary" id="edit-profile" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                    <button type="submit" class="btn btn-primary" id="update-profile" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 border-left">
                            <div id="avatar_image">
                                <center>
                                     @if($user_details->image == '')
                                    <img width="50%" src="{{ asset('images/profile/placeholder.jpg')}}">
                                     @else
                                     <img src="{{ asset('attachments/'.\Auth::user()->company_id.'/C-CP/'.$user_details->image)}}">
                                    @endif
                                </center>
                            </div>
                            <br>
                            
                            <div class="m-b-10"></div>
                          

                            <div id="image_deleted_text"></div>
                            <div class="m-b-10"></div>
                            <div class="form-group">
                                <label>Role:</label>
                                <p class="role_name_view">{{ $user_details->role_name }}</p>
                                <p class="role_id_view" style="display: none">{{ $user_details->role_id }}</p>
                                <span id="role_name_view" style="display: none">
                                    <select class="form-control" name="role_id" class="role_append">
                                        @foreach($role_details as $key => $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </span> 
                                <label>Department:</label>
                                <p class="department_name_view">{{ $user_details->department_name }}</p>
                                <span id="department_name_view"></span> 
                                <label>PR Department:</label>
                                @if($pr_departments)
                                    @foreach($pr_departments as $depts)
                                    <p class="pr_department_name_view">{{ $depts->details->name }}</p>
                                    @endforeach
                                @else
                                <p class="pr_department_name_view">Unassigned</p>
                                @endif
                                <?php $counter = 0;?>
                                <span id="pr_department_view" style="display: none;">

                                    <select class="form-control pr_department" name="pr_department[]" multiple="multiple">
                                        <option></option>
                                        @foreach ($department_details as $dept_list)
                                            @if(in_array($dept_list->id,$pr_dept_id,TRUE))
                                                <option value="{{ $dept_list->id }}" selected="selected">{{ $dept_list->name }}</option>
                                            @else
                                                <option value="{{ $dept_list->id }}">{{ $dept_list->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </span> 
                                <label>Position:</label>
                                <p class="position_name_view">{{ $user_details->position_name }}</p>
                                <span id="position_name_view"></span> 
                                
                                <label>Status: </label><br><span id="status_view"></span><span class="status_view"> <?php echo $status = ($user_details->status == 1 ? 'ACTIVE' : 'INACTIVE')?></span>
                            <br>
                             <br>
                             <b>Latest Activity date: </b><br> {{ date('M d, Y H:i:s', strtotime($user_details->updated_at)) }}
                             <b>Register date: </b><br> {{ date('M d, Y H:i:s', strtotime($user_details->created_at)) }}
                            <br />
                            <b>Register IP: </b><br> {{ $user_details->registered_ip }}
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
            <!-- end card-body -->								
        </div>
        <!-- end card -->					
    </div>
    <!-- end col -->	
</div>
<!-- end row -->
<script src="{{ asset('/js/bootstrap3-typeahead.js') }}"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
            $('div').on('blur', '.department', function(e){
                $('.notification').show();
            })
        // }
        $('div').on('keyup', '#check-username',function(e){
            var username = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            e.preventDefault();
            $.ajax({
                url: '/users/checkUsername',
                type: "POST",
                data: {
                        username:username   
                    },
                success: function(response) {
                    $('.username').html('');
                    if (response >= 1) {
                       $('.username').append('<p style="font-size: 12px; color: #ff5d48; margin-top: 5px;">Username already exists.</p>');
                       $('#update-profile').attr('disabled', 'disabled');
                    }else{
                       $('#update-profile').removeAttr('disabled');
                    }
                }
            });
            e.preventDefault();
            return false;
        });       
                
        if( $('#edit-profile').length > 0 ) {
            $('#edit-profile').click(function(e){
                $('#role_name_view').show();
                $('#pr_department_view').show();
                $('#module_view').show();
                $('.pr_department').select2({
                        placeholder: "Select a Department"
                });
                $('.allowed_acces').select2({
                        placeholder: "Select Module"
                });
                $(document).on('select2:select', '.pr_department', function(e){
                    e.preventDefault();
                    return false;
                });
                var first_name_view     = $('.first_name_view').text();
                var middle_name_view    = $('.middle_name_view').text();
                var last_name_view      = $('.last_name_view').text();
                var name_extension_view = $('.name_extension_view').text();
                var username_view       = $('.username_view').text();
                var email_view          = $('.email_view').text();
                var view_mode_view      = $('.view_mode_view').text();
                var role_name_view      = $('.role_name_view').text();
                // var pr_dept             = $('.department_name_view').text(); 
                var role_id_view        = $('.role_id_view').text();
                var department_name_view = $('.department_name_view').text();
                var position_name_view  = $('.position_name_view').text();
                var status_view         = $('.status_view').text();

                $('#first_name_view').append('<input type="text" class="form-control" name="first_name" value="'+first_name_view+'"/>');
                $('#middle_name_view').append('<input type="text" class="form-control" name="middle_name" value="'+middle_name_view+'"/>');
                $('#last_name_view').append('<input type="text" class="form-control" name="last_name" value="'+last_name_view+'"/>');
                $('#name_extension_view').append('<input type="text" class="form-control" name="name_extension" value="'+name_extension_view+'"/>');
                $('#username_view').append('<input type="text" class="form-control" name="username" value="'+username_view+'" id="check-username"/>');
                $('#email_view').append('<input type="email" class="form-control" name="email" value="'+email_view+'" required/>');
                // View only
                if(view_mode_view == 'ON')
                {
                    $('#view_mode_view').append(''+
                        '<select class="form-control view-mode" name="view_mode">'+
                            '<option value="'+view_mode_view+'" selected>'+view_mode_view+'</option>'+
                            '<option value="OFF">OFF</option>'+
                        '</select>'
                        );
                    $('#module_view').show();

                }
                else
                {
                    $('#view_mode_view').append(''+
                        '<select class="form-control view-mode" name="view_mode">'+
                            '<option value="'+view_mode_view+'" selected>'+view_mode_view+'</option>'+
                            '<option value="ON">ON</option>'+
                        '</select>'
                        );   
                    $('#module_view').hide();
                }
                $('#department_name_view').append('<input type="text" class="form-control typeahead-department-view department" name="department_name" value="'+department_name_view+'"/>');
                $('#position_name_view').append('<input type="text" class="form-control typeahead-position-view" name="position_name" value="'+position_name_view+'"/>');
                $('#status_view').append(''+
                                            '<select class="form-control" name="status">'+
                                                '<option value="'+status_view+'">'+status_view+'</option>'+
                                                '<option value="ACTIVE">ACTIVE</option>'+
                                                '<option value="INACTIVE">INACTIVE</option>'+
                                            '</select>'
                                            );

                $('#role_name_view select').val(role_id_view);

                $('.first_name_view').hide();
                $('.middle_name_view').hide();
                $('.last_name_view').hide();
                $('.name_extension_view').hide();
                $('.username_view').hide();
                $('.email_view').hide();
                $('.view_mode_view').hide();
                $('.department_name_view').hide();
                $('.position_name_view').hide();
                $('.status_view').hide();
                $('.role_name_view').hide();
                $('.pr_department_name_view').hide();
                $('.view_mode_name_view').hide();
                $('.module_name_view').hide();
                $(this).hide();
                $('#update-profile').show();
                if( $('.typeahead-position-view').length > 0 ) {
                   $.get('/users/getPosition', function(a) {
                       $(".typeahead-position-view").typeahead({
                           items: "all",
                           autoSelect: false,
                           source: a
                       });
                   }, "json");
               }

               if( $('.typeahead-department-view').length > 0 ) {
                   $.get('/users/getDepartment', function(a) {
                       $(".typeahead-department-view").typeahead({
                           items: "all",
                           autoSelect: false,
                           source: a
                       });
                   }, "json");
               }
            })
        }
        $('div').on('change','.view-mode',function(e){
        // $('.view-mode').change(function(e){
            var view_mode = $('.view-mode').val();
            if(view_mode == 'ON')
            {
                $('#module_view').show();
                $('.view_mode_name_view').hide();
            }
            else
            {
                $('.view_mode_name_view').show();
                $('#module_view').hide();
            }

            e.preventDefault();
            return false;
        });
        $('div').on('click', '#update-profile', function(e){
            swal({
                title: "Are you sure?",
                text: "Once saved, you will not be able to change your key information!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSave) => {
                if (willSave) {
                    var formData = $('#edit-profile-form').serialize();
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        url: '/users/updateUserDetails',
                        type: "POST",
                        data: formData,
                        success: function(response) {
                            console.log (response);
                            if (response != '') {
                                swal("Your account has been updated!", {
                                    icon: "success",
                                });
                                $('.get-users-table').html(response);
                            }
                            else {
                                swal("This has already been applied", {
                                    icon: "error",
                                });
                            }
                        }
                    });
                } else {
                    swal("Save Aborted");
                }
            });
            e.preventDefault();
            return false;
        })


    })
</script>