@extends('layouts.app')
@section('content') 
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ strtoupper(str_replace("-", " ", Request::segment(2))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <h3><i class="fa fa-table"></i> All Company <span class="pull-right"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add_company_modal"><i class="fa fa-user-plus" aria-hidden="true"></i></button></span></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-companies-table">
                            <table id="companies-table" class="table table-striped table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Company ID</th>
                                        <th>Company Name</th>
                                        <th>User Count</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('customer_management.modal.add_company_modal')
@include('customer_management.modal.view_company_details_modal')
<script type="text/javascript">
	//function in calling datatable ajax
    function getCompaniesTable(){
      var table = $('#companies-table').DataTable({
               ajax: {
                   url: '/admin/customer-management/getCompanies',
                   dataSrc: ''
               },
               columns: [ 
                   { data: '#' },
                   { data: 'company_id' },
                   { data: 'company_details' },
                   { data: 'user_count' },
                   { data: 'status' },
                   { data: 'actions' }
               ]
           });
    }
    
    $(document).ready(function(){
        //ca;; datatable ajax
        getCompaniesTable();
        //search each column datatable
	    $('#companies-table thead th').each(function(e) {
	        var title = $(this).text();
	        $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
	    });
	    $('#companies-table').DataTable().columns().every(function() {
	        var that = this;
	        $( 'input', this.header() ).on('keyup change', function () {
	            if ( that.search() !== this.value ) {
	            that
	                .search( this.value )
	                .draw();
	            }
	        });
	    });

	    //view companies modal
	    $('div').on('click', '.view-company-details', function(e){
	    	$('#view-company-details').modal('show');
	    	$('.customer-details').html('');
	    	var id = this.id;
	    	$.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            })
            $.ajax({
               url: '/admin/customer-management/viewCustomerDetails',
               type: "POST",
               dataType: "HTML",
               data: {id:id},
               beforeSend: function() {
               		$('.loader').show();
               },
               success: function(response) {
                   if (response != '') {
                   		$('.customer-details').html(response);
                   }
               },
               complete: function() {
               		$('.loader').hide();
               }
            });
	    	e.preventDefault();
	    	return false;
	    });
    })
    
</script>
@endsection