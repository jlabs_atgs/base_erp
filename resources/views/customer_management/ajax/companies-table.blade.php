
<table id="companies-table" class="table table-striped table-bordered">
    <thead class="thead-global">
        <tr>
            <th>#</th>
            <th>Company ID</th>
            <th>Company Name</th>
            <th>User Count</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
	//function in calling datatable ajax
    function getCompaniesTable(){
      var table = $('#companies-table').DataTable({
               ajax: {
                   url: '/admin/customer-management/getCompanies',
                   dataSrc: ''
               },
               columns: [ 
                   { data: '#' },
                   { data: 'company_id' },
                   { data: 'company_details' },
                   { data: 'user_count' },
                   { data: 'status' },
                   { data: 'actions' }
               ]
           });
    }
    
    $(document).ready(function(){
        //ca;; datatable ajax
        getCompaniesTable();
        //search each column datatable
	    $('#companies-table thead th').each(function(e) {
	        var title = $(this).text();
	        $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
	    });
	    $('#companies-table').DataTable().columns().every(function() {
	        var that = this;
	        $( 'input', this.header() ).on('keyup change', function () {
	            if ( that.search() !== this.value ) {
	            that
	                .search( this.value )
	                .draw();
	            }
	        });
	    });
    })
    
</script>