<div class="row">
	<div class="col-md-6">
		<input type="hidden" name="user-id-approval" id="user-id-approval-approver" value="{{ $user_id }}" readonly="readonly">
		<table class="table table-condensed select-approver-table datatable">
			<thead>
				<tr>
					<th>Select Approver</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="select-approver">
				@if($all_users)
				@foreach($all_users as $key => $value)
				<tr>
					<td>
				        {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}
				    </td>
				    <td>
				    	<button id="{{ $value->id }}" class="btn btn-secondary btn-circle btn-sm add-approver-to-user" title="Add Approver"><i class="fa fa-plus"></i></button>
				    </td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="col-md-6 get-approver-approval">
		<div class="row">
			<table class="table table-condensed approver-table datatable">
				<thead>
					<tr>
						<th>Approver ID</th>
						<th>Assigned Approver</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="approver-approval">
					@if($approver)
					@foreach($approver as $key => $value)
					<tr>
						<td class="approver_id">{{ $value->id }}</td>
						<td>
					       <font color="green">&#9398</font> {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}
					    </td>
					    <td><button class="deselect-approver btn btn-danger btn-circle btn-sm" title="Deselect"><i class="fa fa-minus"></i></button></td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<button id="save-user-approver-approval" class="btn btn-primary btn-circle btn-sm pull-right" disabled="disabled"><i class="fa fa-floppy-o"></i></button></button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<span id="approver-changes" style="font-size: 10px; color: red;"></span>
			</div>
		</div>
	</div>	 
</div>
<script src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datatable').DataTable();
		$('.select-approver-table').on('click', '.add-approver-to-user', function(e){
			$('span#approver-changes').html('');
			var approver_id = this.id;
			var name 		= $(this).closest("tr").find('td:eq(0)').text();
			$(this).closest("tr").remove();
			e.preventDefault();
			$('tbody#approver-approval').append(''+
					'<tr>'+
						'<td class="approver_id">'+approver_id+'</td>'+
						'<td>'+name+'</td>'+
						'<td><button class="deselect-approver btn btn-danger btn-circle btn-sm" title="Deselect Approver"><i class="fa fa-minus"></i></button></td>'+
					'</tr>');
			$('#save-user-approver-approval').removeAttr('disabled');
			$('span#approver-changes').append('Changes has been made, please save your work.');
			e.preventDefault();
		})

		$(".approver-table").on('click', '.deselect-approver', function(e){
			$('span#approver-changes').html('');
			var approver_id = $(this).closest("tr").find('td:eq(0)').text();
    		var name 		= $(this).closest("tr").find('td:eq(1)').text();
		    $(this).closest('tr').remove();
		    $('tbody#select-approver').append(''+
				'<tr>'+
					'<td>'+name+'</td>'+
					'<td><button id="'+approver_id+'" class="btn btn-secondary btn-circle btn-sm add-approver-to-user" title="Add Approver"><i class="fa fa-plus"></i></button></td>'+
				'</tr>');
		    $('#save-user-approver-approval').removeAttr('disabled');
		    $('span#approver-changes').append('Changes has been made, please save your work.');
		    e.preventDefault();
		    return false;

		});

	    $('div').on('click', '#save-user-approver-approval', function(e){
	    	var user_id 	= $('#user-id-approval-approver').val();
	    	var approval_id = $('#approval_id').val();
	    	var array 		= [];
            $('.approver-table tr').each(function (a, b) {
                var approver_id = $('.approver_id', b).text();
                array.push({ approver_id:approver_id });
               
            });
            var newArray2 = array.filter(function(v){return v!==''});
            swal({
	            title: "Confirm Saving",
	            text: "Are you sure you want to save the following users as approver for the selected user?",
	            icon: "warning",
	            buttons: true,
	            dangerMode: true,
	        })
	        .then((willSave) => {
	            if (willSave) {
	            	$.ajaxSetup({
		                headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                }
		            })
		            $.ajax({
		                url: '/approval/saveUserApproverApproval',
		                type: "POST",
		                data: {
		                	approver_id:newArray2,
		                    user_id:user_id,
		                    approval_id:approval_id
		                },
		                success: function(response) {
		                    if (response != '') {
		                    	$('#save-user-approver-approval').attr('disabled', 'disabled');
								$('span#approver-changes').html('');
		                        swal("Approver for the Selected user has been saved!", {
		                          icon: "success",
		                        });
		                    }
		                }
		            }) 
	            } else {
	              swal("Save Aborted!");
	            } 
	        });  
	    	e.preventDefault();
    		return false;
	    });
	})
</script>

