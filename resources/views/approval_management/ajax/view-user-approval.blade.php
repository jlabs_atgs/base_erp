<div class="row">
	<div class="col-md-12">
		<br>
		<h6><font color="orange">&#169</font> SET CHECKER</h6>
		<hr>
	</div>
	<div class="col-md-4">
		<input type="hidden" name="approval_id" id="approval_id" readonly="readonly" value="{{ $approval_id }}">
		<table class="table table-condensed datatable">
			<thead>
				<tr>
					<th>Preparer</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if($users)
					@foreach($users as $key => $value)
					<tr>
						<td>&#169 {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}</td>
						<td><button id="{{ $value->id }}" title="Select User" class="btn btn-secondary
							-circle btn-sm view-select-user"><i class="fa fa-check-square-o" aria-hidden="true"></i></button></td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="col-md-8 view-user-checker">
	</div>
</div>
<hr>
<br>
<hr>
<div class="row">
	<div class="col-md-12">
		<br>
		<h6><font color="green">&#9398</font> SET APPROVER</h6>
		<hr>
	</div>
	<div class="col-md-4">
		<table class="table table-condensed datatable">
			<thead>
				<tr>
					<th>Preparer</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if($users)
					@foreach($users as $key => $value)
					<tr>
						<td>&#9413 {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}</td>
						<td><button id="{{ $value->id }}" title="Select User" class="btn btn-secondary btn-circle btn-sm view-select-user2"><i class="fa fa-check-square-o" aria-hidden="true"></i></button></td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="col-md-8 view-user-approver">
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datatable').DataTable();

		if( $('.view-select-user').length > 0 ) {
			$('.view-select-user').click(function(e){
				$('.view-select-user').css({
				   'color' : '',
				   'background-color' : ''
				});
				var user_id 	= this.id;
				var approval_id = $('#approval_id').val();
				$(this).css({
				   'color' : 'white',
				   'background-color' : 'blue'
				});
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: '/approval/viewUserChecker',
                    type: "POST",
                    data: {
                        user_id:user_id,
                        approval_id:approval_id
                    },
                    beforeSend: function() {
	                    $('span#result').html('');
	                    $('.view-user-approval-button').attr('disabled','disabled');
	                    $('span#loader').append(''+
	                       '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
	                   );
	                },
                    success: function(response) {
                        if (response != '') {
                            $('.view-user-checker').html(response);
                        }
                    },
	                complete: function() {
	                    $('span#loader').html('');
	                }
                });

				e.preventDefault();
				return false;
			});

			
		}

		if( $('.view-select-user2').length > 0 ) {
			$('.view-select-user2').click(function(e){
				$('.view-select-user2').css({
				   'color' : '',
				   'background-color' : ''
				});
				var user_id 	= this.id;
				var approval_id = $('#approval_id').val();
				$(this).css({
				   'color' : 'white',
				   'background-color' : 'blue'
				});
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: '/approval/viewUserApprover',
                    type: "POST",
                    data: {
                        user_id:user_id,
                        approval_id:approval_id
                    },
                    beforeSend: function() {
	                    $('span#result').html('');
	                    $('.view-user-approval-button').attr('disabled','disabled');
	                    $('span#loader').append(''+
	                       '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
	                   );
	                },
                    success: function(response) {
                        if (response != '') {
                            $('.view-user-approver').html(response);
                        }
                    },
	                complete: function() {
	                    $('span#loader').html('');
	                }
                });
				e.preventDefault();
				return false;
			});
		}
	})
</script>