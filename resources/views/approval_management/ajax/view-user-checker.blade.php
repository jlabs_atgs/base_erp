<div class="row">
	<input type="hidden" name="user-id-approval" id="view-user-id-approval" value="{{ $user_id }}" readonly="readonly">
	<div class="col-md-6">
		<table class="table table-condensed select-checker-table datatable">
			<thead>
				<tr>
					<th>Select Checker</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="select-checker">
				@if($all_users)
				@foreach($all_users as $key => $value)
				<tr>
					<td>
				        {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}
				    </td>
				    <td>
				    	<button id="{{ $value->id }}" class="btn btn-secondary btn-circle btn-sm add-checker-to-user" title="Add Checker"><i class="fa fa-plus"></i></button>
				    </td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="col-md-6 get-checker-approval">
		<div class="row">
			<table class="table table-condensed checker-table datatable">
				<thead>
					<tr>
						<th>Checker ID</th>
						<th>Assigned Checker</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="checker-approval">
					@if($checker)
					@foreach($checker as $key => $value)
					<tr>
						<td class="checker_id">{{ $value->id }}</td>
						<td>
					        <font color="orange">&#169</font> {{ $value->last_name }}, {{ $value->first_name }} {{ $value->middle_name }}
					    </td>
					    <td><button class="deselect-checker btn btn-danger btn-circle btn-sm" title="Deselect"><i class="fa fa-minus"></i></button></td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<button id="save-user-checker-approval" class="btn btn-primary btn-circle btn-sm pull-right"><i class="fa fa-floppy-o"></i></button></button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<span id="checker-changes" style="font-size: 10px; color: red;"></span>
			</div>
		</div>
	</div>	 
</div>
<script src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datatable').DataTable();
		$('.select-checker-table').on('click', '.add-checker-to-user', function(e){
			$('span#checker-changes').html('');
			var checker_id 	= this.id;
			var name 		= $(this).closest("tr").find('td:eq(0)').text();
			$(this).closest("tr").remove();
			e.preventDefault();
			$('tbody#checker-approval').append(''+
					'<tr>'+
						'<td class="checker_id">'+checker_id+'</td>'+
						'<td>'+name+'</td>'+
						'<td><button class="deselect-checker btn btn-danger btn-circle btn-sm" title="Deselect Checker"><i class="fa fa-minus"></i></button></td>'+
					'</tr>');
			$('#save-user-checker-approval').removeAttr('disabled');
			$('span#checker-changes').append('Changes has been made, please save your work.');
			e.preventDefault();
		})

		$(".checker-table").on('click', '.deselect-checker', function(e){
			$('span#checker-changes').html('');
			var checker_id 	= $(this).closest("tr").find('td:eq(0)').text();
    		var name 		= $(this).closest("tr").find('td:eq(1)').text();
		    $(this).closest('tr').remove();
		    $('tbody#select-checker').append(''+
				'<tr>'+
					'<td>'+name+'</td>'+
					'<td><button id="'+checker_id+'" class="btn btn-secondary btn-circle btn-sm add-checker-to-user" title="Add Checker"><i class="fa fa-plus"></i></button></td>'+
				'</tr>');
		    $('#save-user-checker-approval').removeAttr('disabled');
			$('span#checker-changes').append('Changes has been made, please save your work.');
		    e.preventDefault();
		    return false;

		});

	    $('div').on('click', '#save-user-checker-approval', function(e){
	    	var user_id 	= $('#view-user-id-approval').val();
	    	var approval_id = $('#approval_id').val();
	    	var array 		= [];
            $('.checker-table tr').each(function (a, b) {
                var checker_id = $('.checker_id', b).text();
                array.push({ checker_id:checker_id });
               
            });
            var newArray = array.filter(function(v){return v!==''});
            swal({
	            title: "Confirm Saving",
	            text: "Are you sure you want to save the following users as checker for the selected user?",
	            icon: "warning",
	            buttons: true,
	            dangerMode: true,
	          })
	        .then((willSave) => {
	            if (willSave) {
	            	$.ajaxSetup({
		                headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                }
		            })
		            $.ajax({
		                url: '/approval/saveUserCheckerApproval',
		                type: "POST",
		                data: {
		                	checker_id:newArray,
		                    user_id:user_id,
		                    approval_id:approval_id
		                },
		                oLanguage: {
		                    sSearch: ''
		                },
		                success: function(response) {
		                    if (response != '') {
		                    	$('#save-user-checker-approval').attr('disabled', 'disabled');
								$('span#checker-changes').html('');
		                        swal("Checker for the Selected user has been saved!", {
		                          icon: "success",
		                        });
		                    }
		                }
		            }) 
	            } else {
	              swal("Save Aborted!");
	            } 
	            $('.dataTables_filter input').attr("placeholder", "Search Records");
	        });  
	    	e.preventDefault();
    		return false;
	    });
	})
</script>

