<label>Select Department</label>
<select name="department_id" class="form-control check-approval" id="department_id">
        <option value="">Select Department</option>
	@foreach($departments as $key => $value)
		<option value="{{ $value->id }}">{{ $value->name }}</option>
	@endforeach
</select>