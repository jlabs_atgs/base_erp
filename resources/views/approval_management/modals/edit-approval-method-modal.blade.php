<style type="text/css">
    .selection{
        width: 100% !important;
        max-height: 100% !important; 
    }
    .modal-xl{
        width: 100% !important;
        max-width: 100% !important;
    }
</style>
<div class="modal fade" id="edit-approval-method-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Edit Approval Method</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 view-user-approval"></div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#view-approval-method-modal').on('hidden.bs.modal', function (e) {
            $('.view-user-approval').html('');
            $('.get-user-approval').html('');
            e.preventDefault();
            return false;
        })
    })
</script>