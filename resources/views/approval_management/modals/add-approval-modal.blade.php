<div class="modal fade custom-modal" id="approval-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Add Approval </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <span id="result" class="text-danger" style="font-size: 10px"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group get-function">
                            <label>Select Function</label>
                            <select name="function_id" class="form-control check-approval" id="function_id">
                                <option value="">Select Function</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group get-department">
                            <label>Select Department</label>
                            <select name="department_id" class="form-control check-approval" id="department_id">
                                <option value="">Select Department</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1" style="margin-top: 27px;">
                        <button class="get-user-approval-button btn bg-orange btn-circle btn-md" title="create" disabled="disabled"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <center><span id="loader" style="color:green; margin-top: 27px;"></span></center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 get-user-approval"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="offset-md-10">
                    <button class="btn btn-success btn-circle btn-md" id="finish-approval" disabled="disabled" title="Complete Approval Method"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
    function getFunctions(){
        $.ajax({
            url: '/approval/getFunctions',
            type: "GET",
            success: function(response) {
                if (response != '') {
                    $('.get-function').html(response);
                }
            }
        });
    }

    function getDepartments(){
        $.ajax({
            url: '/approval/getDepartments',
            type: "GET",
            success: function(response) {
                if (response != '') {
                    $('.get-department').html(response);
                }
            }
        });
    }

    function getApprovalsTable(){
      var table = $('.approval-table').DataTable({
               ajax: {
                   url: '/approval/getApprovals',
                   dataSrc: ''
               },
                oLanguage: {
                    sSearch: ''
                },
               columns: [ 
                   { data: '#' },
                   { data: 'function' },
                   { data: 'department' },
                   { data: 'reference_number' },
                   { data: 'actions' }
               ]

           });
       $('.dataTables_filter input').attr("placeholder", "Search Records");
    }

    $(document).ready(function(){
        setTimeout(
            function() 
            {
                getFunctions();
                getDepartments();
            }, 2000
        );

        $('div').on('change', '.check-approval', function(e){
            var department_id   = $('#department_id').val();
            var function_id     = $('#function_id').val();
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: '/approval/checkApproval',
                type: "POST",
                data: {
                    function_id:function_id,
                    department_id:department_id
                },
                beforeSend: function() {
                    $('span#result').html('');
                    $('.get-user-approval-button').attr('disabled','disabled');
                    $('span#loader').append(''+
                       '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
                   );
                },
                success: function(response) {
                    if (response >= 1) {
                        $('.get-user-approval-button').attr('disabled','disabled');
                        $('span#result').append(''+
                            'the selected department/function has an already existing approval method!'
                        );
                    }else{
                        $('.get-user-approval-button').removeAttr('disabled');
                    }
                },
                complete: function() {
                    $('span#loader').html('');
                }
            });
            e.preventDefault();
            return false;
        });

        if( $('.get-user-approval-button').length > 0 ) {
            $('.get-user-approval-button').click(function(e){
                var department_id   = $('#department_id').val();
                var function_id     = $('#function_id').val();
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: '/approval/addUserApproval',
                    type: "POST",
                    data: {
                        function_id:function_id,
                        department_id:department_id
                    },
                    beforeSend: function() {
                        $('span#result').html('');
                        $('.get-user-approval-button').attr('disabled','disabled');
                        $('span#loader').append(''+
                           '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
                       );
                    },
                    success: function(response) {
                        if (response != '') {
                            $('.get-user-approval-button').attr('disabled','disabled');
                            $('.check-approval').attr('disabled','disabled');
                            $('#finish-approval').removeAttr('disabled');
                            $('.get-user-approval').html(response);
                        }
                    },
                    complete: function() {
                        $('span#loader').html('');
                    }
                });
                e.preventDefault();
                return false;
            });
        }

        if( $('#finish-approval').length > 0 ) {
            $('#finish-approval').click(function(e){
                swal({
                    title: "Complete Approval Method",
                    text: "Are you sure you want to complete the current approval method?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willSave) => {
                    if (willSave) {
                        $('.check-approval').removeAttr('disabled');
                        $('.get-user-approval').html("");
                        $('.approval-table').DataTable().destroy();
                        getApprovalsTable();
                        $('#finish-approval').attr('disabled', 'disabled');
                        swal("Approval Method for the following department/function has been completed!", {
                          icon: "success",
                        });
                    } else {
                      swal("Completion Aborted!");
                    } 
                });  
                e.preventDefault();
                return false;
            });
        }
    })
</script>
