@extends('layouts.app')
@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ strtoupper(str_replace("-", " ", Request::segment(1))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-thumbs-up"></i> Approvals List<span class="pull-right"><button id="add-approval-level" class="btn btn-primary btn-sm" title="Add"><i class="fa fa-plus"></i></button></span></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-approval-table">
                           <table class="table table-striped table-bordered table-condensed approval-table">
                              <thead class="thead-global">
                                 <tr>
                                    <th>#</th>
                                    <th>Function</th>
                                    <th>Department</th>
                                    <th>Reference Number</th>
                                    <th>Actions</th>
                                 </tr>
                              </thead>
                              <tbody></tbody>
                           </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('approval_management.modals.add-approval-modal')
@include('approval_management.modals.edit-approval-method-modal')
@include('approval_management.modals.view-approval-method-modal')
<script type="text/javascript">
   function getApprovalsTable(){
      var table = $('.approval-table').DataTable({
               ajax: {
                   url: '/approval/getApprovals',
                   dataSrc: ''
               },
                oLanguage: {
                sSearch: ''
                },
               columns: [ 
                   { data: '#' },
                   { data: 'function' },
                   { data: 'department' },
                   { data: 'reference_number' },
                   { data: 'actions' }
               ]

           });
      
           $('.dataTables_filter input').attr("placeholder", "Search Records");
    }
   $(document).ready(function(){
      
      setTimeout(
         function() 
         {
            getApprovalsTable();
            //search each column datatable
            $('.approval-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('.approval-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
         }, 1000);
      if( $('#add-approval-level').length > 0 ) {
      	$('#add-approval-level').click(function(e){
      		$('#approval-modal').modal('show');
      		return false;
      		e.preventDefault();
      	});
      }

      $('div').on('click', '.edit-approval-method', function(e){
        $('#edit-approval-method-modal').modal('show');
        var id = this.id;
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            url: '/approval/getViewUserApproval',
            type: "POST",
            data: {
                approval_id:id
            },
            beforeSend: function() {
                $('span#result').html('');
                $('span#loader').append(''+
                   '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
               );
            },
            success: function(response) {
                if (response != '') {
                    $('.view-user-approval').html(response);
                }
            },
            complete: function() {
                $('span#loader').html('');
            }
        });
        e.preventDefault();
        return false;
      })

      $('div').on('click', '.view-approval-method', function(e){
        var permission_name = $(this).data('permission');
        var department_name = $(this).data('department');
        $('#view-approval-method-modal').modal('show');
        var id = this.id;
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            url: '/approval/getViewApproval',
            type: "POST",
            data: {
                approval_id:id,
                permission_name:permission_name,
                department_name:department_name,
            },
            beforeSend: function() {
                $('span#result').html('');
                $('span#loader').append(''+
                   '<i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw"></i>'
               );
            },
            success: function(response) {
                if (response != '') {
                    $('.view-approval').html(response);
                }
            },
            complete: function() {
                $('span#loader').html('');
            }
        });
        e.preventDefault();
        return false;
      })

   })
</script>
@endsection