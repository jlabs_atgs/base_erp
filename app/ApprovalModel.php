<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class ApprovalModel extends Model
{
    public static function getApprovals()
    {
        return DB::table('approval_table as a')
                ->select(DB::raw('
                            MAX(af.permission_name) as permission_name, 
                            MAX(d.name) as department_name, 
                            MAX(a.id) as approval_id
                        '))
                ->leftJoin('checker_approval_table as c', 'a.id', '=', 'c.approval_id')
                ->leftJoin('approver_approval_table as r', 'a.id', '=', 'r.approval_id')
                ->leftJoin('user_roles_permission as af', 'a.function_id', '=', 'af.id')
                ->leftJoin('departments as d', 'a.department_id', 'd.id')
                ->where('a.company_id', Auth::user()->company_id)
                ->groupBy('a.id')
                ->get()->toArray();
    }

    public static function getFunctions()
    {
        return DB::table('user_roles_permission')
                ->whereNotIn('id', [0,1,2,15,16,17,18,19,20,21,22,23,24,37,39])
                ->get();
    }

    public static function getDepartments()
    {
        return DB::table('departments')
                ->where('company_id', Auth::user()->company_id)
                ->get();
    }

    public static function checkApproval($data)
    {
        return DB::table('approval_table')
                ->where('function_id', $data['function_id'])
                ->where('department_id', $data['department_id'])
                ->where('company_id', Auth::user()->company_id)
                ->get();
    }

    public static function addUserApproval($data)
    {
        DB::table('approval_table')
                ->insert([
                    'function_id'   => $data['function_id'],
                    'department_id' => $data['department_id'],
                    'company_id'     => Auth::user()->company_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => Auth::user()->id
                ]);
        return DB::getPdo()->lastInsertId();
    }

    public static function getUserApproval($data)
    {
        return DB::table('users')
                ->where('department_id', $data['department_id'])
                ->get();
    }

    public static function getAllUsers()
    {
        return DB::table('users')
                ->select('id', 'first_name', 'middle_name', 'last_name')
                ->where('company_id', Auth::user()->company_id)
                ->get()->toArray();
    }

    public static function getUserChecker($data)
    {
        return DB::table('checker_approval_table as c')
                ->leftJoin('users as u', 'c.user_id', '=', 'u.id')
                ->where('c.approval_id', $data['approval_id'])
                ->where('user_id', $data['user_id'])
                ->get()->toArray();
    }

    public static function getUserCheckerArray($data)
    {
        return DB::table('users')
                ->select('id', 'first_name', 'middle_name', 'last_name')
                ->where('id', $data)
                ->get()->toArray();
    }

    public static function getUserArray($data)
    {
        return DB::table('users')
                ->whereNotIn('id', $data)
                ->where('company_id', Auth::user()->company_id)
                ->get()->toArray();
    }

    public static function saveUserChecker($data, $checker_id)
    {
        $check = DB::table('checker_approval_table')
                    ->where('approval_id', $data['approval_id'])
                    ->where('user_id', $data['user_id'])
                    ->get();
        if ($check->count() >= 1) {
            return DB::table('checker_approval_table')
                    ->where('approval_id', $data['approval_id'])
                    ->where('user_id', $data['user_id'])
                    ->update([
                        'user_id'       => $data['user_id'],
                        'checker_id'    => $checker_id,
                        'approval_id'   => $data['approval_id'],
                        'company_id'     => Auth::user()->company_id,
                        'updated_at'    => date('Y-m-d H:i:s'),
                        'updated_by'    => Auth::user()->id
                    ]);
        }else{
            return DB::table('checker_approval_table')
                ->insert([
                    'user_id'       => $data['user_id'],
                    'checker_id'    => $checker_id,
                    'approval_id'   => $data['approval_id'],
                    'company_id'     => Auth::user()->company_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => Auth::user()->id
                ]);
        }
    }

    public static function getUserApprover($data)
    {
        return DB::table('approver_approval_table as c')
                ->leftJoin('users as u', 'c.user_id', '=', 'u.id')
                ->where('c.approval_id', $data['approval_id'])
                ->where('user_id', $data['user_id'])
                ->get()->toArray();
    }

    public static function saveUserApprover($data, $approver_id)
    {
        $check = DB::table('approver_approval_table')
                    ->where('approval_id', $data['approval_id'])
                    ->where('user_id', $data['user_id'])
                    ->get();
        if ($check->count() >= 1) {
            return DB::table('approver_approval_table')
                    ->where('approval_id', $data['approval_id'])
                    ->where('user_id', $data['user_id'])
                    ->update([
                        'user_id'       => $data['user_id'],
                        'approver_id'    => $approver_id,
                        'approval_id'   => $data['approval_id'],
                        'company_id'     => Auth::user()->company_id,
                        'updated_at'    => date('Y-m-d H:i:s'),
                        'updated_by'    => Auth::user()->id
                    ]);
        }else{
            return DB::table('approver_approval_table')
                ->insert([
                    'user_id'       => $data['user_id'],
                    'approver_id'    => $approver_id,
                    'approval_id'   => $data['approval_id'],
                    'company_id'     => Auth::user()->company_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => Auth::user()->id
                ]);
        }
    }

    public static function getApprovalDepartmentID($data)
    {
        return DB::table('approval_table')
                ->select('department_id')
                ->where('id', $data['approval_id'])
                ->get();
    }

    public static function getCheckerApproval($data)
    {
        return DB::table('checker_approval_table')
                ->select('user_id', 'checker_id')
                ->where('approval_id', $data['approval_id'])
                ->get()->toArray();
    }

    public static function checkUser($data)
    {
        return DB::table('users')
                ->where('id', $data)
                ->get();
    }

    public static function checkCheckers($data)
    {
        return DB::table('users')
                ->select('last_name', 'first_name', 'middle_name')
                ->whereIn('id', $data)
                ->where('company_id', Auth::user()->company_id)
                ->get()->toArray();
    }

    public static function getApproverApproval($data)
    {
        return DB::table('approver_approval_table')
                ->select('user_id', 'approver_id')
                ->where('approval_id', $data['approval_id'])
                ->get()->toArray();
    }

    public static function getTransactionStatus($data)
    {
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->where('company_id', Auth::user()->company_id)
                ->first();
    }

    //aproval function for transactions
    public static function getApprovalIDTransactions($data)
    {
        return DB::table('approval_table')
                ->select('id')
                ->where('function_id', $data)
                ->where('company_id', Auth::user()->company_id)
                ->get()->toArray();
    }

    public static function getCheckersRequisition($data, $user)
    {
       
        return DB::table('checker_approval_table')
                ->select('checker_id')
                ->whereIn('approval_id', $data)
                ->where('user_id', $user)
                ->get()->toArray();
    }

    public static function getApproversRequisition($data, $user)
    {
        return DB::table('approver_approval_table')
                ->select('approver_id')
                ->whereIn('approval_id', $data)
                ->where('user_id', $user)
                ->get()->toArray();
    }

    public static function submitTransaction($data)
    {
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'approval_status' => $data['approval_id'],
                    'submitted_at'    => date('Y-m-d H:i:s')
                ]);
    }

    public static function checkTransaction($data)
    {
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'approval_status'   => $data['approval_id'],
                    'checked_at'        => date('Y-m-d H:i:s'),
                    'checked_by'        => Auth::user()->id
                ]);
    }

    public static function approveTransaction($data)
    {
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'approval_status'   => $data['approval_id'],
                    'approved_at'       => date('Y-m-d H:i:s'),
                    'approved_by'       => Auth::user()->id
                ]);
    }

    public static function cancelTransaction($data)
    {
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'approval_status'   => $data['approval_id'],
                    'canceled_at'       => date('Y-m-d H:i:s'),
                    'canceled_by'       => Auth::user()->id
                ]);
    }

    public static function returnSubmittedTransaction($data)
    {
        $res = DB::table('return_remarks')
                ->insert([
                    'transaction_id' => $data['id'],
                    'function_id'    => $data['function_id'],
                    'approval_id'    => $data['approval'],
                    'remarks'        => $data['return-remarks'],
                    'created_by'     => Auth::user()->id,
                    'created_at'     => date('Y-m-d H:i:s')
                ]);
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'return_tag'        => '1',
                    'return_by'         => Auth::user()->id,
                    'return_at'         => date('Y-m-m H:i:s'), 
                    'approval_status'   => $data['approval_id'],
                    'submitted_at'      => NULL,
                    'checked_at'        => NULL,
                    'approved_at'       => NULL,
                    'approved_by'       => NULL,
                    'checked_by'        => NULL
                ]);
    }

    public static function addCommentApproved($data)
    {
        $res = DB::table('return_remarks')
                ->insert([
                    'transaction_id' => $data['id'],
                    'function_id'    => $data['function_id'],
                    'approval_id'    => $data['approval'],
                    'remarks'        => $data['return-remarks'],
                    'created_by'     => Auth::user()->id,
                    'created_at'     => date('Y-m-d H:i:s')
                ]);
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'approval_status'   => $data['approval_id'],
                    'approved_by'       => Auth::user()->id,
                    'approved_at'       => date('Y-m-d H:i:s')
                ]);
    }

    public static function returnCheckedTransaction($data)
    {
        $res = DB::table('return_remarks')
                ->insert([
                    'transaction_id' => $data['id'],
                    'function_id'    => $data['function_id'],
                    'approval_id'    => $data['approval'],
                    'remarks'        => $data['return-remarks'],
                    'created_by'     => Auth::user()->id,
                    'created_at'     => date('Y-m-d H:i:s')
                ]);
        return DB::table($data['table'])
                ->where('id', $data['id'])
                ->update([
                    'return_tag'        => '1',
                    'return_by'         => Auth::user()->id,
                    'return_at'         => date('Y-m-m H:i:s'), 
                    'approval_status'   => $data['approval_id'],
                    'checked_at'        => NULL
                ]);
    }

    public static function checkIfItemExists($item,$total_quantity,$column_items,$column_main)
    {
        $check_main = DB::table('inventory_quantity_report')
                ->where('company_id', Auth::user()->company_id)
                ->get();

        if ($check_main->count() >= 1) {
            //update main table
            $update_main    =  DB::table('inventory_quantity_report')
                                    ->where('company_id',Auth::user()->company_id)
                                    ->update([
                                        $column_main            => $total_quantity
                                ]);

            $check = DB::table('inventory_quantity_report_items')
            ->select(DB::raw(
                    'inventory_id,
                     warehouse_id'
            ))
            // ->leftJoin('inventory_adjustment as i_a', 'i_a_i.inventory_adjustment_id', '=', 'i_a.id')
            // ->where('i_a.approval_status', 3)
            ->where('inventory_id', $item->inventory_id)
            ->where('company_id', Auth::user()->company_id)
            ->where('warehouse_id', $item->warehouse_id)
            ->groupBy('inventory_id','warehouse_id')
            ->get();
            $iqr_id_items = DB::table('inventory_quantity_report')
                            ->select('id')
                            ->where('company_id',Auth::user()->company_id)
                            ->first();
            if ($check->count() >= 1) {
                return DB::table('inventory_quantity_report_items')
                        ->where('inventory_id', $item->inventory_id)
                        ->where('company_id', Auth::user()->company_id)
                        ->where('warehouse_id', $item->warehouse_id)
                        ->update([
                            $column_items     => $item->quantity
                        ]);

            }else{
                //insert items
                
                return DB::table('inventory_quantity_report_items')
                    ->insert([
                        'inventory_quantity_report_id'      => $iqr_id_items->id,
                        $column_items                       => $item->quantity,
                        'warehouse_id'                      => $item->warehouse_id,
                        'unit'                              => $item->unit, 
                        'inventory_id'                      => $item->inventory_id,
                        'company_id'                        => Auth::user()->company_id
                ]); 
            }    
        }else{
            //insert main table
            $accounting_period      = account_period()[0];
            $split                  = explode('/',$accounting_period);
            $year                   = $split[2];
            $insert_main    =   DB::table('inventory_quantity_report')
                                    ->insert([
                                        $column_main            => $total_quantity,
                                        'company_id'            => Auth::user()->company_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                        'accounting_year'       => $year,
                                ]);
            $iqr_id = DB::getPdo()->lastInsertId();
            // dd($insert_main);
            if ($insert_main) {
                return  DB::table('inventory_quantity_report_items')
                                    ->insert([
                                        'inventory_quantity_report_id'      => $iqr_id,
                                        $column_items                       => $item->quantity,
                                        'warehouse_id'                      => $item->warehouse_id,
                                        'unit'                              => $item->unit, 
                                        'inventory_id'                      => $item->inventory_id,
                                        'company_id'                        => Auth::user()->company_id,
                                ]); 
            }
        }
    }
    public static function getCheckerApprovalsByDepartment($data, $department_id)
    {
        return DB::table('approval_table as a')
                ->select('c.id as id')
                ->leftJoin('checker_approval_table as c', 'a.id', '=', 'c.approval_id')
                ->where('c.user_id', $data['user_id'])
                ->where('a.department_id', '!=', $department_id)
                ->get()->toArray();
    }

    public static function getApproverApprovalsByDepartment($data, $department_id)
    {
        return DB::table('approval_table as a')
                ->select('c.id as id')
                ->leftJoin('approver_approval_table as c', 'a.id', '=', 'c.approval_id')
                ->where('c.user_id', $data['user_id'])
                ->where('a.department_id', '!=', $department_id)
                ->get()->toArray();
    }

    public static function deleteAllWithDifferentDepartment($id, $table)
    {
        return DB::table($table)
                ->where('id', $id)
                ->delete();
    }

    public static function addedComments($data)
    {
        $res = DB::table('return_remarks')
                ->insert([
                    'transaction_id' => $data['transaction_id'],
                    'function_id'    => $data['function_id'],
                    'approval_id'    => $data['approval'],
                    'remarks'        => $data['comment_remarks'],
                    'created_by'     => Auth::user()->id,
                    'created_at'     => date('Y-m-d H:i:s')
                ]);
    }

    public static function returnRemarks($data)
    {
        return DB::table('return_remarks as r')
                ->select('r.remarks as remarks', 'r.created_at as created_at', 'u.name as name')
                ->leftJoin($data['table'].' as p_r', 'r.transaction_id', '=', 'p_r.id')
                ->leftJoin('users as u', 'r.created_by', '=', 'u.id')
                ->where('p_r.id', $data['transaction_id'])
                ->where('r.function_id', $data['function_id'])
                ->orderBy('r.created_at', 'DESC')
                ->get()->toArray();
    }  

    public static function getApprovalID($function_id)
    {
        return DB::table('approval_table')
                ->where('function_id', $function_id)
                ->where('company_id', Auth::user()->company_id)
                ->get()->toArray();
    }

    public static function getCheckers($user_id, $approval_id)
    {
        return DB::table('checker_approval_table')
                ->whereIn('approval_id', $approval_id)
                ->where('user_id', $user_id)
                ->first();  
                // ->get()->toArray(); 
    }
    public static function getApproverByFunctionId($function_id)
    {
        return DB::table('approval_table as ap_table')
                ->leftJoin('approver_approval_table as approver_app_table','approver_app_table.approval_id','=','ap_table.id')
                ->where('ap_table.function_id',$function_id)
                ->get();
    }
}
