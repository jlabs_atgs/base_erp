<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user){
        $this->logoutOtherAccountUsingThis($user);
    }

    protected function logoutOtherAccountUsingThis($user){
        $new_session_id = Session::getId();

        $last_session_id = Session::getHandler()->read($user->session_id);

        if($last_session_id){
            if(Session::getHandler()->destroy($user->session_id)){
                //sessions were destroyed
            }
        }
        // $user->saveSession($new_session_id);
        // dd($new_session_id);
    }

    protected function sendLoginResponse(Request $request){
        $request->session()->regenerate();

        $previous_session = Auth::user()->session_id;

        if($previous_session){
            Session::getHandler()->destroy($previous_session);
        }

        Auth::user()->session_id = Session::getId();

        Auth::user()->save();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user()) ?: redirect()->intended($this->redirectPath());
    }
}
