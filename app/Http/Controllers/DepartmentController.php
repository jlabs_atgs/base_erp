<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\DepartmentModel;
use DB;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $function;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->function = "Department";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (\Auth::user()->status == 1) {
            return view('departments.index');
        }else{
            return redirect('/inactive');
        }
    }
    public function getAllDepartment()
    {
        $result = DepartmentModel::getDepartment();
        return response()->json($result);
    }
    public function getDepartment()
    {
        $counter    = 1;
        $data       = DepartmentModel::getDepartment();
        if ($data) {
            $counter    = 1;
            foreach ($data as $key => $value) {
                $result[]  = array(
                    '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                    'id'                    => $value->id,
                    'name'                  => '<p>'.$value->name.'</p>',
                    'action'                => '<button class="btn btn-primary btn-sm view-department-details" id="'.$value->id.'" title="View"><i class="tiny material-icons">visibility</i></button>',
                );
            }
        }else{
            $result[]  = array(
                '#'                     => 0,
                'id'                    => 'n/a',
                'name'                  => 'n/a',
                'status'                => 'n/a',
                'action'                => 'n/a',
            );
        }
        return response()->json($result);
    }
    public function addDepartment(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $data['function_name']  = $this->function;
            $result                 = DepartmentModel::addDepartment($data);
            $data['number']         = $result;
            //audit trail function
            trailCreate($data);
            return $result;
        });       
        return response()->json($result);
    }
    public function getDepartmentDetails(Request $request)
    {
        $data                       = $request->all();
        $data['department']         = DepartmentModel::getDepartmentDetails($data);
        return view('departments.ajax.department-details',$data);

    }
    public function updateDepartment(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $data['function_name']  = $this->function;
            $result                 = DepartmentModel::updateDepartment($data);
            $data['number']         = $data['id'];
            //audit trail function
            trailUpdate($data);
            return $result;
        });       
        return response()->json($result);
    }
    public function getDepartmentLimit(Request $request)
    {   
        $page                           = $request->get('page');
        $term                           = $request->get('term');
        $resultCount                    = 10;
        $offset                         = ($page - 1) * $resultCount;
        $vendor                         = DepartmentModel::getDepartmentLimit($term, $offset, $resultCount);
        $count                          = DepartmentModel::countDepartment();
        $endCount                       = $offset + $resultCount;
        $morePages                      = $endCount > $count;

        $results = array(
          "results" => $vendor,
          "pagination" => array(
            "more" => $morePages
          )
        );
        return response()->json($results);
        
    }
}
