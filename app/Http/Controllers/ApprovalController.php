<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ApprovalModel;
use App\PurchaseInvoiceModel;
use App\PurchaseOrderModel;
use App\PaymentModel;
use App\SaleInvoiceModel;
use App\SaleOrderModel;
use App\CollectionModel;
use App\CanvassSummaryModel;
use App\PurchaseRequisitionModel;
use App\InventoryAdjustmentModel;
use App\WarehouseReportModel;
use App\OtherPayableModel;
use App\OtherReceivableModel;
use App\WarehouseTransferModel;
use App\PaymentOthersModel;
use App\RequestForPaymentModel;
use App\Events\ApprovalPushNotification;
use App\GlobalModel;
use App\CashAdvancePayment;
use App\ReceivingReportModel;
use App\DeliveryReportModel;
use DB;

class ApprovalController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $function;

    public function __construct()
    {
        $this->middleware('auth');
        $this->function = "Approval";
    }


    public function index()
    {
        return view('approval_management.index');
    }

    public function getApprovals()
    {
        $counter    = 1;
        $data       = ApprovalModel::getApprovals();
        if ($data) {
            foreach ($data as $key => $value) {
                $result[]   = array(
                    '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                    'function'              => '<p>'.$value->permission_name.'</p>',
                    'department'            => '<p>'.$value->department_name.'</p>',
                    'reference_number'      => ''.$value->approval_id.'',
                    'actions'               => '<button class="btn btn-primary view-approval-method btn-sm btn-circle-sm" id="'.$value->approval_id.'" title="View" data-permission="'.$value->permission_name.'" data-department="'.$value->department_name.'"><i class="fa fa-eye" aria-hidden="true"></i></button> <button class="btn btn-info edit-approval-method btn-sm btn-circle-sm" id="'.$value->approval_id.'" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'
                );
            }
        }else{
            $result[] = array(
                    '#'                     => 0,
                    'function'              => '<p>N/A</p>',
                    'department'            => '<p>N/A</p>',
                    'reference_number'      => 0,
                    'actions'               => 'N/A'
                );
        }
        
        return response()->json($result);
    }

    public function getFunctions()
    {
        $result = ApprovalModel::getFunctions();
        $returnHTML = view('approval_management.ajax.get-function',
            [
                'functions'       => $result
            ])->render();
        echo $returnHTML;
    }

    public function getDepartments()
    {
        $result = ApprovalModel::getDepartments();
        $returnHTML = view('approval_management.ajax.get-department',
            [
                'departments'       => $result
            ])->render();
        echo $returnHTML;
    }

    public function checkApproval(Request $request)
    {
        $data       =   $request->all();
        $result     =   ApprovalModel::checkApproval($data);
        return response()->json($result->count());
    }

    public function addUserApproval(Request $request)
    {
        $data       =   $request->all();
        $approval_id=   ApprovalModel::addUserApproval($data);
        $users      =   ApprovalModel::getUserApproval($data);
        $returnHTML = view('approval_management.ajax.get-user-approval',
            [
                'users'       => $users,
                'approval_id' => $approval_id
            ])->render();
        echo $returnHTML;
    }

    public function getUserChecker(Request $request)
    {
        $data       =   $request->all();
        $all_users  =   ApprovalModel::getAllUsers();
        $checker    =   ApprovalModel::getUserChecker($data);
        if (!empty($checker)) {
            $checkers   =   explode(',', $checker[0]->checker_id);
            // dd($checkers);
            foreach ($checkers as $key => $value) {
                $array[] = ApprovalModel::getUserCheckerArray($value);
            }
            foreach ($array as $key => $value) {
                $array2[]   = $value[0];
            }
            $user  = ApprovalModel::getUserArray($checkers);
        }else{
            $array2 = '';
            $user  = $all_users;
        }
        
        $returnHTML =   view('approval_management.ajax.get-user-checker',
            [
                'checker'   => $array2,
                'all_users' => $user,
                'user_id'   => $data['user_id']
            ])->render();
        echo $returnHTML;
    }

    public function viewUserChecker(Request $request)
    {
        $data       =   $request->all();
        $all_users  =   ApprovalModel::getAllUsers();
        $checker    =   ApprovalModel::getUserChecker($data);
        if (!empty($checker)) {
            $checkers   =   explode(',', $checker[0]->checker_id);
            // dd($checkers);
            foreach ($checkers as $key => $value) {
                $array[] = ApprovalModel::getUserCheckerArray($value);
            }
            if ($array[0]) {
                foreach ($array as $key => $value) {
                    $array2[]   = $value[0];
                }
                $user  = ApprovalModel::getUserArray($checkers);
            }else{
                $array2 = '';
                $user  = $all_users;
            }
        }else{
            $array2 = '';
            $user  = $all_users;
        }
        
        $returnHTML =   view('approval_management.ajax.view-user-checker',
            [
                'checker'   => $array2,
                'all_users' => $user,
                'user_id'   => $data['user_id']
            ])->render();
        echo $returnHTML;
    }

    public function saveUserCheckerApproval(Request $request)
    {
        $data       =   $request->all();
        foreach ($data['checker_id'] as $key => $value) {
            $checker_id[] = $value['checker_id'];
        }
        $checker_id =   array_filter($checker_id);
        $checker_id =   implode(',', $checker_id);
        $result     =   ApprovalModel::saveUserChecker($data, $checker_id);
        return response()->json($result);
    }

    public function getUserApprover(Request $request)
    {
        $data       =   $request->all();
        $all_users  =   ApprovalModel::getAllUsers();
        $approver    =   ApprovalModel::getUserApprover($data);
        if (!empty($approver)) {
            $approvers   =   explode(',', $approver[0]->approver_id);
            // dd($checkers);
            foreach ($approvers as $key => $value) {
                $array[] = ApprovalModel::getUserCheckerArray($value);
            }
            foreach ($array as $key => $value) {
                $array2[]   = $value[0];
            }
            $user  = ApprovalModel::getUserArray($approvers);
        }else{
            $array2 = '';
            $user  = $all_users;
        }
        
        $returnHTML =   view('approval_management.ajax.get-user-approver',
            [
                'approver'   => $array2,
                'all_users' => $user,
                'user_id'   => $data['user_id']
            ])->render();
        echo $returnHTML;
    }

    public function viewUserApprover(Request $request)
    {
        $data       =   $request->all();
        $all_users  =   ApprovalModel::getAllUsers();
        $approver    =   ApprovalModel::getUserApprover($data);
        if (!empty($approver)) {
            $approvers   =   explode(',', $approver[0]->approver_id);
            // dd($checkers);
            foreach ($approvers as $key => $value) {
                $array[] = ApprovalModel::getUserCheckerArray($value);
            }
            if ($array[0]) {
                foreach ($array as $key => $value) {
                    $array2[]   = $value[0];
                }
                $user  = ApprovalModel::getUserArray($approvers);
            }else{
                $array2 = '';
                $user  = $all_users;                
            }
        }else{
            $array2 = '';
            $user  = $all_users;
        }
        
        $returnHTML =   view('approval_management.ajax.view-user-approver',
            [
                'approver'   => $array2,
                'all_users' => $user,
                'user_id'   => $data['user_id']
            ])->render();
        echo $returnHTML;
    }

    public function saveUserApproverApproval(Request $request)
    {
        $data       =   $request->all();
        foreach ($data['approver_id'] as $key => $value) {
            $approver_id[] = $value['approver_id'];
        }
        $approver_id =   array_filter($approver_id);
        $approver_id =   implode(',', $approver_id);
        $result     =   ApprovalModel::saveUserApprover($data, $approver_id);
        return response()->json($result);
    }

    public function getViewUserApproval(Request $request)
    {
        $data       =   $request->all();
        $department_id = ApprovalModel::getApprovalDepartmentID($data);
        $array['department_id'] = $department_id[0]->department_id;
        $users      =   ApprovalModel::getUserApproval($array);
        $returnHTML = view('approval_management.ajax.view-user-approval',
            [
                'users'       => $users,
                'approval_id' => $data['approval_id']
            ])->render();
        echo $returnHTML;
    }

    public function getViewApproval(Request $request)
    {
        $data         =     $request->all();
        $checker      =     ApprovalModel::getCheckerApproval($data);
        $approver     =     ApprovalModel::getApproverApproval($data);
        $approvals    =     array_merge($checker, $approver);
        $defined_approval   =   $this->getDefinedApprovals($approvals);
        $result = array();
        foreach($defined_approval as $value){
            $user_id = $value->user_id;
            if(isset($result[$user_id]))
                $index = ((count($result[$user_id]) - 1) / 2) + 1;
            else
                $index = 1;

            $result[$user_id]['user_id'] = $user_id;
            $result[$user_id]['user_name'][$index] = $value->user_name;
            $result[$user_id]['user_checkers'][$index] = $value->user_checkers;   
            $result[$user_id]['user_approvers'][$index] = $value->user_approvers;        
        }
        $result = array_values($result);
        $returnHTML = view('approval_management.ajax.view-approval',
            [
                'approval'              => $result,
                'permission_name'       => $data['permission_name'],
                'department_name'       => $data['department_name']
            ])->render();
        echo $returnHTML;
    }

    private function getDefinedApprovals($approvals)
    {
        $data   =   [];
        foreach ($approvals as $key => $value) {
            $items                  =   new \stdClass();
            if (isset($value->checker_id)) {
                $checkers           =   explode(',', $value->checker_id);
                $user_checkers      =   ApprovalModel::checkCheckers($checkers);
            }else{
                $user_checkers      =   [];
            }
            if (isset($value->approver_id)) {
                $approvers          =   explode(',', $value->approver_id);
                $user_approvers     =   ApprovalModel::checkCheckers($approvers);
            }else{
                $user_approvers     =   [];
            }
            $user_name              =   ApprovalModel::checkUser($value->user_id);
            $items->user_id         =   $value->user_id;
            $items->user_name       =   $user_name[0]->last_name.', '.$user_name[0]->first_name.' '.$user_name[0]->middle_name; 
            $items->user_checkers   =   $user_checkers;
            $items->user_approvers  =   $user_approvers;
            array_push($data, $items);
        }
        return $data;
    }

    public function getApprovalStatus(Request $request)
    {
        $data = $request->all();
        $approval_id    = ApprovalModel::getApprovalIDTransactions($data['function_id']);
        if ($approval_id) {
            foreach ($approval_id as $key => $value) {
                $ap_id[]    = $value->id;
            }
            $checkers       = ApprovalModel::getCheckersRequisition($ap_id, $data['user_id']);
            foreach ($checkers as $key => $value) {
                $c_id[]     = $value->checker_id;
            }
            $approvers      = ApprovalModel::getApproversRequisition($ap_id, $data['user_id']);
            foreach ($approvers as $key => $value) {
                $a_id[]     = $value->approver_id;
            }
            $checker_id     = (isset($c_id) ? array_unique(explode(',', $c_id[0])) : []);
            $approver_id    = (isset($a_id) ? array_unique(explode(',', $a_id[0])) : []);
            $check_status   =   ApprovalModel::getTransactionStatus($data);
            if ($check_status->approval_status == 0) {
                if ($data['function_id'] != 3) {
                    if ($data['user_id'] == Auth::user()->id) {
                        $result = ' 
                                     <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" title="Attachment" aria-expanded="false" aria-controls="collapseExample">
                                         <i class="fa fa-paperclip fa-sm"></i>
                                     </a>
                                    <button type="button" class="btn btn-primary btn-sm edit-transaction" id="edit-purchase-order-button" title="Edit">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </button>

                                    <button class="btn btn-danger btn-sm delete-transaction" title="Delete"> 
                                        <i class="fa fa-trash"></i>
                                    </button>&nbsp;
                                    <button class="btn btn-primary btn-sm submit-transaction" id="1" title="Submit">
                                        <i class="fa fa-paper-plane"></i>
                                    </button>';
                    }else{
                        $result = '
                                    <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                        <i class="fa fa-paperclip fa-sm"></i>
                                    </a>
                                    <p>Created!</p>';
                    }
                }else{
                    if($data['warning'] == 1)
                    {
                        if ($data['user_id'] == Auth::user()->id) {
                            $result = ' 
                                         <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" title="Attachment" aria-expanded="false" aria-controls="collapseExample">
                                             <i class="fa fa-paperclip fa-sm"></i>
                                         </a>
                                        <button type="button" class="btn btn-primary btn-sm edit-transaction" id="edit-purchase-order-button" title="Edit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </button>

                                        <button class="btn btn-danger btn-sm delete-transaction" title="Delete"> 
                                            <i class="fa fa-trash"></i>
                                        </button>&nbsp;
                                        <button class="btn btn-primary btn-sm submit-transaction" id="1" title="Submit" disabled="disabled">
                                            <i class="fa fa-paper-plane"></i>
                                        </button>';
                        }else{
                            $result = '
                                        <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                            <i class="fa fa-paperclip fa-sm"></i>
                                        </a>
                                        <p>Created!</p>';
                        }
                    }
                    else
                    {
                        if ($data['user_id'] == Auth::user()->id) {
                            $result = ' 
                                         <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" title="Attachment" aria-expanded="false" aria-controls="collapseExample">
                                             <i class="fa fa-paperclip fa-sm"></i>
                                         </a>
                                        <button type="button" class="btn btn-primary btn-sm edit-transaction" id="edit-purchase-order-button" title="Edit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </button>

                                        <button class="btn btn-danger btn-sm delete-transaction" title="Delete"> 
                                            <i class="fa fa-trash"></i>
                                        </button>&nbsp;
                                        <button class="btn btn-primary btn-sm submit-transaction" id="1" title="Submit">
                                            <i class="fa fa-paper-plane"></i>
                                        </button>';
                        }else{
                            $result = '
                                        <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                            <i class="fa fa-paperclip fa-sm"></i>
                                        </a>
                                        <p>Created!</p>';
                        }   
                    }
                }
            }elseif ($check_status->approval_status == 1) {
                if (in_array(Auth::user()->id, $checker_id)) {
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <button class="btn btn-success btn-sm check-transaction" id="2" title="Check"><i class="fa fa-check"></i></button>&nbsp;
                                <button class="btn btn-warning btn-sm return-submitted-transaction" id="0" data-approval="'.$ap_id[0].'" title="Return"><i class="fa fa-undo"></i></button>';
                }else{
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>
                                <p>Submitted!</p>';
                }
            }elseif ($check_status->approval_status == 2) {

                if (in_array(Auth::user()->id, $approver_id)) {
                    if($data['function_id'] == "40"){
                        $result = '
                                    <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                        <i class="fa fa-paperclip fa-sm"></i>
                                    </a>&nbsp;
                                    <button class="btn btn-success btn-sm approve-transaction" id="3" data-comment="'.$ap_id[0].'" title="Approve"><i class="fa fa-thumbs-o-up"></i></button>&nbsp;
                                    <button class="btn btn-warning btn-sm return-checked-transaction" id="1" data-approval="'.$ap_id[0].'" title="Return"><i class="fa fa-undo"></i></button>&nbsp;
                                    <button class="btn btn-danger btn-sm cancel-transaction" id="4" title="Cancel"><i class="fa fa-times"></i></button>';
                    }
                    else {
                        $result = '
                                    <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                        <i class="fa fa-paperclip fa-sm"></i>
                                    </a>&nbsp;
                                    <button class="btn btn-success btn-sm approve-transaction" id="3" title="Approve"><i class="fa fa-thumbs-o-up"></i></button>&nbsp;
                                    <button class="btn btn-warning btn-sm return-checked-transaction" id="1" data-approval="'.$ap_id[0].'" title="Return"><i class="fa fa-undo"></i></button>&nbsp;
                                    <button class="btn btn-danger btn-sm cancel-transaction" id="4" title="Cancel"><i class="fa fa-times"></i></button>';

                    }

                }else{
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>
                                <p>Checked!</p>';
                }
            }elseif ($check_status->approval_status == 3) {
                if (in_array($data['function_id'], [3,5,7,9,11,13,14,25,27,28])) {
                    if (in_array(Auth::user()->id, $approver_id)) {
                        $result = '
                                    <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                        <i class="fa fa-paperclip fa-sm"></i>
                                    </a>&nbsp;
                                    <button class="btn btn-warning btn-sm return-submitted-transaction" id="0" data-approval="'.$ap_id[0].'" title="Return"><i class="fa fa-undo"></i></button>
                                    <p>Approved!</p>';
                    }else{
                        $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <p>Approved!</p>';
                    }
                }else{
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <p>Approved!</p>';
                }
            }elseif ($check_status->approval_status == 4) {
                $result = '
                            <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                <i class="fa fa-paperclip fa-sm"></i>
                            </a>&nbsp;
                            <p>Cancelled!</p>';
            }
        }else{
            $check_status   =   ApprovalModel::getTransactionStatus($data);
            if ($check_status->approval_status == 0) {
                if ($data['user_id'] == Auth::user()->id) {
                    $result = ' 
                                 <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <button type="button" class="btn btn-primary btn-sm edit-transaction" id="edit-purchase-order-button" title="Edit">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </button>

                                <button class="btn btn-danger btn-sm delete-transaction" title="Delete"> 
                                    <i class="fa fa-trash"></i>
                                </button>&nbsp;
                                <button class="btn btn-primary btn-sm submit-transaction" id="1" title="Submit">
                                    <i class="fa fa-paper-plane"></i>
                                </button>';
                }else{
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <p>Created!</p>';
                }
            }elseif ($check_status->approval_status == 1) {
                if ($data['user_id'] == Auth::user()->id) {
                    $result = '
                                 <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <button class="btn btn-success btn-sm approve-transaction" id="3" title="Approve"><i class="fa fa-thumbs-o-up"></i></button>&nbsp;
                                <button class="btn btn-warning btn-sm return-submitted-transaction" id="0" data-approval="0" title="Return"><i class="fa fa-undo"></i></button>&nbsp;
                                <button class="btn btn-danger btn-sm cancel-transaction" id="4" title="Cancel"><i class="fa fa-times"></i></button>';
                }else{
                    $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                    <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <p>Submitted!</p>';
                }      
            }elseif ($check_status->approval_status == 3) {

                $result = '
                                <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                     <i class="fa fa-paperclip fa-sm"></i>
                                </a>&nbsp;
                                <p>Approved!</p>';
            }elseif ($check_status->approval_status == 4) {
                $result = '
                            <a class="btn btn-success btn-sm btn-circle-sm text-white" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Attachment">
                                <i class="fa fa-paperclip fa-sm"></i>
                            </a>&nbsp;
                            <p>Cancelled!</p>
                            ';
            }
        }
        return response()->json($result);
    }

    public function submitTransaction(Request $request)
    {
        $data       = $request->all();
        $array      = [
                        'table'             => $data['table'],
                        'user'              => Auth::user()->id,
                        'transaction_id'    => $data['id'],
                        'approval_id'       => $data['approval_id']
                    ]; 
        $result  = ApprovalModel::submitTransaction($data);
        event(new ApprovalPushNotification($array));
        return response()->json($result);
    }

    public function checkTransaction(Request $request)
    {
        $data    = $request->all();
        // $array      = [
        //                 'table'             => $data['table'],
        //                 'user'              => Auth::user()->id,
        //                 'transaction_id'    => $data['id'],
        //                 'approval_id'       => $data['approval_id']
        //             ]; 
        $result  = ApprovalModel::checkTransaction($data);
        // event(new ApprovalPushNotification($array));
        return response()->json($result);
    }

    public function approveTransaction(Request $request)
    {
        $data    = $request->all();
        // $array      = [
        //                 'table'             => $data['table'],
        //                 'user'              => Auth::user()->id,
        //                 'transaction_id'    => $data['id'],
        //                 'approval_id'       => $data['approval_id']
        //             ]; 
        $result  = ApprovalModel::approveTransaction($data);
        // event(new ApprovalPushNotification($array));
        return response()->json($result);
    }
    //special case for PI
    public function approvePurchaseInvoiceTransaction(Request $request)
    {
        $data                           = $request->all();
        // dd($data);
        $result                         = ApprovalModel::approveTransaction($data);
        $array                          = PurchaseInvoiceModel::getPurchaseInvoiceDetails($data['id']);
        $po                             = PurchaseOrderModel::getPurchaseOrderDetails($array->purchase_order_id);
        if($po->purchase_requisition_id)
        {
            $pr                          = PurchaseRequisitionModel::getPurchaseRequisitionDetails(($po->purchase_requisition_id) ? $po->purchase_requisition_id : '');
            $purchase_requisition_items     = PurchaseRequisitionModel::getPurchaseRequisitionItemsDetails($pr->id);

            $pr_items                       = $this->getPRITems($purchase_requisition_items);
            // dd($pr_items);
            foreach($pr_items as $key => $value)
            {
                $i[]        = $value->result;
            }
            if (in_array(false, $i, true)) 
            {
                $pr_status = '2';
                $update_pr_status = PurchaseRequisitionModel::updatePurchaseRequisitionStatus($pr->id,$pr_status);

            }else
            {
                $pr_status = '3';
                $update_pr_status = PurchaseRequisitionModel::updatePurchaseRequisitionStatus($pr->id,$pr_status);
            }
        }
        // $pi_quantities                  = PurchaseInvoiceModel::getPIQuantities($array->id);
        //
        $pi_items                       = PurchaseInvoiceModel::getPurchaseInvoiceItemsDetailsApproved();
        if($pi_items)
        {
            $total_quantity = 0;
            foreach($pi_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($pi_items,$total_quantity,'pi_quantity','pi_total_qty');
        }
        
        if (round($array->balance) == 0.00) {
            $status = '2';
            $update = PurchaseOrderModel::updatePurchseOrderStatus($array->purchase_order_id, $status);
        }else{
            $status = '1';
            $update = PurchaseOrderModel::updatePurchseOrderStatus($array->purchase_order_id, $status);
        }
        return response()->json($result);
    }
    private function getPRITems($purchase_requisition_items)
    {
        $array = [];

        foreach($purchase_requisition_items as $key => $value)
        {
            $items                  = new \stdClass();
            $po_items                = PurchaseOrderModel::getFullyDeliveredItems($value->inventory_id,$value->purchase_requisition_id);
            // dd($po_items);
            if($po_items)
            {
                $qty                =   $this->getPIQuantity($po_items);
                $total_quantity = 0;
                foreach ($qty as $q => $qt) {
                    $total_quantity += $qt->qty;
                }
                if((float)$value->quantity == (float)$total_quantity)
                {
                    $items->result   = true;
                }
                else
                {
                    $items->result = false;
                }
            }
            else
            {
                $items->result = false;
            }
            array_push($array,$items);
        }
        return $array;
    }
    private function getRRITems($receiving_report_items,$purchase_order_id)
    {
        $array = [];

        foreach($receiving_report_items as $key => $value)
        {
            $items                   = new \stdClass();
            $rr_quantity             = ReceivingReportModel::getRRQuantity($value->purchase_order_item_id);
            $po_items                = PurchaseOrderModel::getFullyDeliveredItemsForRR($value->inventory_id,$purchase_order_id);
            if($po_items)
            {

                if((float)$rr_quantity->quantity == (float)$po_items[0]->quantity)
                {
                    $items->result   = true;
                }
                else
                {
                    $items->result = false;
                }
            }
            else
            {
                $items->result = false;
            }
            array_push($array,$items);
        }
        return $array;
    }

   
    public function getPIQuantity($po_items)
    {
        $array = [];

        foreach($po_items as $po => $po_item)
        {
            $items                  = new \stdClass();
            $pi_item                = PurchaseInvoiceModel::getPIITemsQuantity($po_item->id,$po_item->inventory_id);
            $items->qty             = isset($pi_item->quantity) ? $pi_item->quantity : 0; 
            array_push($array,$items);
        }
        return $array;
    }
    //special case for OP(IA)
    public function approveOtherPayableTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        $array                          = OtherPayableModel::getOtherPayableDetails($data['id']);
        // $pi_quantities                  = PurchaseInvoiceModel::getPIQuantities($array->id);
        //
        $op_items                       = OtherPayableModel::getOtherPayableItemsDetailsApproved();
        // dd($op_items);
        if($op_items)
        {
            $total_quantity = 0;
            foreach($op_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($op_items,$total_quantity,'op_quantity','op_total_qty');
        }
        
        return response()->json($result);
    }

    //special case for OP(IA)
    public function approveWarehouseTransferTransaction(Request $request)
    {
        $data                                               = $request->all();
        $result                                             = ApprovalModel::approveTransaction($data);
        $warehouse_transfer                                 = WarehouseTransferModel::getWarehouseTransferDetails($data['id']);
        $warehouse_transfer_items_form                      = WarehouseTransferModel::getWarehouseTransferItemsDetailsFromApproved($warehouse_transfer->warehouse_from_id);
        $warehouse_transfer_items_to                        = WarehouseTransferModel::getWarehouseTransferItemsDetailsToApproved($warehouse_transfer->warehouse_to_id);
        if($warehouse_transfer_items_form)
        {
            $total_quantity = 0;
            foreach($warehouse_transfer_items_form as $i => $item)
            {
                $total_quantity += $item->quantity;
            }

                $check_to                                          = $this->checkItemExists($warehouse_transfer_items_form,$total_quantity,'wt_deduct','wt_deduct');
           
        }
        if($warehouse_transfer_items_to)
        {
            $total_quantity = 0;
            foreach($warehouse_transfer_items_to as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
                $check                                        = $this->checkItemExists($warehouse_transfer_items_to,$total_quantity,'wt_add','wt_add');
        }
        
        return response()->json($result);
    }

    
    //special case for Payments
    public function approvePaymentTransaction(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data           = $request->all();
            $payables       = PaymentModel::getPayableId($data['id']);
            $result         = ApprovalModel::approveTransaction($data);
            foreach ($payables as $key => $value) {
                $payable_status            = PaymentModel::changePaymentStatus($value); 
                $payable_transactions[]    = PaymentModel::checkPayableTransactionStatus($value);  
            }
            foreach ($payable_transactions as $pt => $payable_transaction) {
                $total_paid_amount  =   $payable_transaction[0]->total_paid_amount;
                $purchase_id        =   $payable_transaction[0]->purchase_id;
                $total_amount_due   =   $payable_transaction[1]->total_amount_due;
                $array[]            =   array(
                                                'purchase_id'       => $purchase_id,
                                                'total_paid_amount' => round((float)$total_paid_amount),
                                                'total_amount_due'  => round((float)$total_amount_due)
                                            );
            }
            $array_res = array_unique($array,SORT_REGULAR);
            foreach ($array_res as $k => $val) {
                if ($val['total_paid_amount'] == $val['total_amount_due']) {
                    $status = '2';    
                } else {
                    $status = '1';
                }
                PaymentModel::updatePayableTransactionStatus($status, $val['purchase_id']);
            }
        });       
        return response()->json($result);
    }
    public function approvePaymentOthersTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        $payment_others                 = PaymentOthersModel::getPaymentOthersDetails($data);

        $request_for_payment            = RequestForPaymentModel::getRFPDetails($payment_others->rfp_id);
        if($request_for_payment)
        {
            RequestForPaymentModel::updateRFPTransactionStatus($request_for_payment->id,'1');
        }

        return response()->json($result);
    }

    public function approveLiquidationTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        $liquidations                   = GlobalModel::getSingleDataTable('cash_advance_payments',$data['id']);
        $liquidated_amount              = CashAdvancePayment::getAllLiquidatedAmount($liquidations->request_for_payment_id);
        $request_for_payment            = GlobalModel::getSingleDataTable('request_for_payment',$liquidations->request_for_payment_id);
        $where = array(
            'id'   => $request_for_payment->id
        ); 
        if($liquidated_amount->liquidated_amount >= $request_for_payment->total_amount)
        {

            $status = array(
                    'ca_status' => '2'
            );
            GlobalModel::changedStatus('request_for_payment',$where,$status);
        }
        else
        {
            $status = array(
                'ca_status' => '1'
            );
            GlobalModel::changedStatus('request_for_payment',$where,$status);
        }

        return response()->json($result);
    }
    //special case for Collections
    public function approveCollectionTransaction(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data               = $request->all();
            $receivables        = CollectionModel::getReceivableId($data['id']);
            $result             = ApprovalModel::approveTransaction($data);
            foreach ($receivables as $key => $value) {
                $receivable_status            = CollectionModel::changeCollectionStatus($value);  
                $receivable_transactions[]    = CollectionModel::checkReceivableTransactionStatus($value);  
            }
            foreach ($receivable_transactions as $pt => $receivable_transaction) {
                $total_paid_amount  =   $receivable_transaction[0]->total_paid_amount;
                $sale_id            =   $receivable_transaction[0]->sale_id;
                $total_amount_due   =   $receivable_transaction[1]->total_amount_due;
                $array[]            =   array(
                                                'sale_id'           => $sale_id,
                                                'total_paid_amount' => round((float)$total_paid_amount),
                                                'total_amount_due'  => round((float)$total_amount_due)
                                            );
            }
            $array_res = array_unique($array,SORT_REGULAR);
            foreach ($array_res as $k => $val) {
                if ($val['total_paid_amount'] == $val['total_amount_due']) {
                    $status = '2';    
                } else {
                    $status = '1';
                }
                CollectionModel::updateReceivableTransactionStatus($status, $val['sale_id']);
            }
        });       
        return response()->json($result);
    }

    

    //special case for SI
    public function approveSaleInvoiceTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        $array                          = SaleInvoiceModel::getSaleInvoiceDetails($data['id']);
        $si_items                       = SaleInvoiceModel::getSaleInvoiceItemsDetailsApproved();
        if($si_items)
        {
            $total_quantity = 0;
            foreach($si_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($si_items,$total_quantity,'si_quantity','si_total_qty');
        }
        if (round($array->balance) == 0.00) {
            $status = '2';
            $update = SaleOrderModel::updateSaleOrderStatus($array->sale_order_id, $status);
        }else{
            $status = '1';
            $update = SaleOrderModel::updateSaleOrderStatus($array->sale_order_id, $status);
        }
        return response()->json($result);
    }
    //special case for OP(IA)
    public function approveOtherReceivableTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        $array                          = OtherReceivableModel::getOtherReceivableDetails($data['id']);
        // $pi_quantities                  = PurchaseInvoiceModel::getPIQuantities($array->id);
        //
        $op_items                       = OtherReceivableModel::getOtherReceivableItemsDetailsApproved();
        // dd($op_items);
        if($op_items)
        {
            $total_quantity = 0;
            foreach($op_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($op_items,$total_quantity,'or_quantity','or_total_qty');
        }
        
        return response()->json($result);
    }
    //special case for PO
    public function approvePurchaseOrderTransaction(Request $request)
    {
        $data       = $request->all();
        $array      = PurchaseOrderModel::getPurchaseOrderDetails($data['id']);
        /* CS */
        if ($array->pr_tag == '1') {
            $status = '2';
            $update = PurchaseRequisitionModel::updatePurchaseRequisitionStatus($array->purchase_requisition_id, $status);
            $result  = ApprovalModel::approveTransaction($data);
            return response()->json($result);
        }
        else
        {
            $result  = ApprovalModel::approveTransaction($data);
            return response()->json($result);
        }
    }
    //special case for CS
    public function approveCanvassSummaryTransaction(Request $request)
    {
        $data       = $request->all();
        $array      = CanvassSummaryModel::getCanvassSummaryDetails($data['id']);
        if ($array) {
            $status = '1';
            $update = PurchaseRequisitionModel::updatePurchaseRequisitionStatus($array->purchase_requisition_id, $status);
        }
        $result  = ApprovalModel::approveTransaction($data);
        return response()->json($result);
    }
    // Special Case for IA
    public function approveInventoryAdjustmentTransaction(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                           = $request->all();
            $result                         = ApprovalModel::approveTransaction($data);
            $array                          = InventoryAdjustmentModel::getInventoryAdjustmentDetails($data['id']);
            if($data['operation'] == "Added")
            {
                $ia_items                       = InventoryAdjustmentModel::getInventoryAdjustmentItemsDetailsAddApproved();
            }
            else
            {
                $ia_items                       = InventoryAdjustmentModel::getInventoryAdjustmentItemsDetailsDeductApproved();
            }
            // dd($ia_items);
            if($ia_items)
            {
                $total_quantity = 0;
                foreach($ia_items as $i => $item)
                {
                    $total_quantity += $item->quantity;
                }
                if($data['operation'] == "Added")
                {

                    $check                      = $this->checkItemExists($ia_items,$total_quantity,'ia_quantity_add','ia_add_total_qty');
                }
                else
                {
                    $check                      = $this->checkItemExists($ia_items,$total_quantity,'ia_quantity_deduct','ia_deduct_total_qty');
                }
            }
            else
            {
                $ia_items_not_approved                       = InventoryAdjustmentModel::getInventoryAdjustmentItemsDetails($array->id);
                $total_quantity = 0;
                foreach($ia_items_not_approved as $key => $value)
                {
                    // inventory count
                    $total_quantity             += $value->quantity;
                    $quantity[]                   = $value->quantity;
                    $unit[]                       = $value->unit;
                    $inventory_id[]               = $value->inventory_id; 
                }
                $iqr_id  = WarehouseReportModel::addInventoryReport($total_quantity);

                foreach ($quantity as $key2 => $value2) {
                    WarehouseReportModel::addInventoryReportItems([
                        'inventory_quantity_report_id'      => $iqr_id,
                        'unit'                              => $unit[$key],
                        'ia_quantity_add'                   => $quantity[$key],
                        'inventory_id'                      => $inventory_id[$key],
                        'warehouse_id'                      => $array->warehouse_id,
                        'company_id'                        => Auth::user()->company_id
                    ]);
                }      
            }
            return $result;
        });
        return response()->json($result);
    }
    //special case for RR
    public function approveRRTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        //
        $receiving_report               = GlobalModel::getSingleDataTable('receiving_reports',$data['id']);
        $purchase_order                 = GlobalModel::getSingleDataTable('purchase_orders',$receiving_report->purchase_order_id);
        $receiving_report_items         = ReceivingReportModel::getRRItemsDetails($receiving_report->id);
        $check_item                       = $this->getRRItems($receiving_report_items,$receiving_report->purchase_order_id);
        foreach($check_item as $key => $value)
        {
            $i[]        = $value->result;
        }
        $where = array(
            'id'    => $receiving_report->purchase_order_id
        );
        /* PARTIALLY DELIVERED */
        if (in_array(false, $i, true)) 
        {
            $status = array(
                'status' => '1'
            );

            $update_pr_status = GlobalModel::changedStatus('purchase_orders',$where, $status);

        }
        /* FULLY DELIVERED */
        else
        {
            $status = array(
                'status' => '2'
            );
            $update_pr_status = GlobalModel::changedStatus('purchase_orders',$where, $status);
        }

        $rr_items                       = ReceivingReportModel::getRRItemsDetailsApproved($receiving_report->purchase_order_id);
        if($rr_items)
        {
            $total_quantity = 0;
            foreach($rr_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($rr_items,$total_quantity,'rr_quantity','rr_total_qty');
        }
        
        return response()->json($result);
    }

    /* DR */

    public function approveDRTransaction(Request $request)
    {
        $data                           = $request->all();
        $result                         = ApprovalModel::approveTransaction($data);
        //
        $delivery_report                = GlobalModel::getSingleDataTable('delivery_reports',$data['id']);
        $salee_order                    = GlobalModel::getSingleDataTable('sale_orders',$delivery_report->sale_order_id);
        $delivery_report_items          = DeliveryReportModel::getDRItemsDetails($delivery_report->id);
        $check_item                     = $this->getDRItems($delivery_report_items,$delivery_report->sale_order_id);
        foreach($check_item as $key => $value)
        {
            $i[]        = $value->result;
        }
        $where = array(
            'id'    => $delivery_report->sale_order_id
        );
        /* PARTIALLY DELIVERED */
        if (in_array(false, $i, true)) 
        {
            $status = array(
                'status' => '1'
            );

            $update_pr_status = GlobalModel::changedStatus('sale_orders',$where, $status);

        }
        /* FULLY DELIVERED */
        else
        {
            $status = array(
                'status' => '2'
            );
            $update_pr_status = GlobalModel::changedStatus('sale_orders',$where, $status);
        }

        $dr_items                       = ReceivingReportModel::getRRItemsDetailsApproved($delivery_report->sale_order_id);
        if($dr_items)
        {
            $total_quantity = 0;
            foreach($dr_items as $i => $item)
            {
                $total_quantity += $item->quantity;
            }
            $check                      = $this->checkItemExists($dr_items,$total_quantity,'dr_quantity','dr_total_qty');
        }
        
        return response()->json($result);
    }

    private function getDRItems($deliver_report_items,$sale_order_id)
    {
        $array = [];

        foreach($deliver_report_items as $key => $value)
        {
            $items                   = new \stdClass();
            $dr_quantity             = DeliveryReportModel::getDRQuantity($value->sale_order_item_id);
            $so_items                = SaleOrderModel::getFullyDeliveredItemsForDR($value->inventory_id,$sale_order_id);
            if($so_items)
            {

                if((float)$dr_quantity->quantity == (float)$so_items[0]->quantity)
                {
                    $items->result   = true;
                }
                else
                {
                    $items->result = false;
                }
            }
            else
            {
                $items->result = false;
            }
            array_push($array,$items);
        }
        return $array;
    }

    // Check Item
    private function checkItemExists($items,$total_quantity,$column_items,$column_main)
    {
        foreach($items as $key => $item)
        {
            ApprovalModel::checkIfItemExists($item,$total_quantity,$column_items,$column_main);

        }
    }
    public function returnSubmittedTransaction(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data    = $request->all();
            $result  = ApprovalModel::returnSubmittedTransaction($data);
            return $result;
        });
        return response()->json($result);
    }

    public function addCommentApproved(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data    = $request->all();
            dd($data);
            $result  = ApprovalModel::addCommentApproved($data);
            return $result;
        });
        return response()->json($result);
    }

    public function returnCheckedTransaction(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {

            $data    = $request->all();
            $result  = ApprovalModel::returnCheckedTransaction($data);
            return $result; 
        });
        return response()->json($result);
    }

    public function cancelTransaction(Request $request)
    {
        $data    = $request->all();
        $result  = ApprovalModel::cancelTransaction($data);
        return response()->json($result);
    }

    public function comments(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data       =   $request->all();
            $added      =   ApprovalModel::addedComments($data);
            $remarks    =   ApprovalModel::returnRemarks($data);
            if ($remarks) {
                $result  =  [];   
                foreach ($remarks as $key => $value) {
                    $item                   =   new \stdClass();
                    $item->created_at       =   date('M d, Y g:i A', strtotime($value->created_at));
                    $item->name             =   $value->name;
                    $item->remarks          =   $value->remarks;
                    array_push($result, $item);
                }
            }else{
                $result = [];
            }
            return $result;
        });
        return response()->json($result);
    }
}