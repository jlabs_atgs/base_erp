<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UserModel;
use App\ProformaModel;
use App\CustomerModel;
use App\RoleModel;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
// use Illum    inate\Http\File;
use File;
use Illuminate\Support\Facades\Validator;
use Session;
use Image;
class CustomerManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->function = "Company Profile";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 0) {
            return view('customer_management.index');
        }
        return redirect('/home');
    }

    public function getCompanies()
    {
        $counter    = 1;
        $data       = UserModel::getAllCompanies();
        foreach ($data as $key => $value) {
            $actual_usercount           = UserModel::getActualUserCount($value->id);
            $result[]   = array(
                '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                'company_id'                    => '<p>'.$value->id.'</p>',
                'company_details'       => '<p>'.$value->company_name.'</p>',
                'user_count'            => '<p style="text-align: right">'.$actual_usercount .' / ' .$value->user_count.'</p>',
                'status'                => $value->status == 1 ? 'ACTIVE' : 'INACTIVE', 
                'actions'               => '<button class="btn btn-primary btn-sm view-company-details" id="'.$value->id.'" title="View"><i class="tiny material-icons">visibility</i></button>'
            );
        }
        return response()->json($result);
    }

    public function checkCustomerUsername(Request $request)
    {
        $data   = $request->all();
        $result = UserModel::checkCustomerUsername($data);
        return response()->json($result->count());
    }

    public function saveCustomer(Request $request)
    {
        DB::transaction( function(&$data) use ($request) {
            $data       = $request->all();
            $company    = array(
                            'company_name'      => $data['company_name'], 
                            'company_email'     => $data['company_email'], 
                            'contact_person'    => $data['contact_person'], 
                            'contact_number'    => $data['contact_number'], 
                            'tin_number'        => $data['tin_number'], 
                            'address_number'    => $data['address_number'], 
                            'street'            => $data['street'], 
                            'barangay'          => $data['barangay'], 
                            'city'              => $data['city'], 
                            'region'            => $data['region'], 
                            'country'           => $data['country'], 
                            'zip_code'          => $data['zip_code'], 
                            'user_count'        => $data['user_count'],
                            'status'            => 1,
                            'created_at'        => date('Y-m-d H:i:s')
                        );
            $company_id    = UserModel::saveCustomerCompanyData($company);
            // File path
            $path = public_path().'/attachments/'.$company_id;
            // if(!Storage::disk('public_attachments')->has($path)){
            //     // Will create folder for the attachment.
            //     Storage::disk('public_attachments')->makeDirectory($path,0777,true);
            // }
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }

            $user       = array(
                            'first_name'        => $data['first_name'], 
                            'middle_name'       => $data['middle_name'], 
                            'last_name'         => $data['last_name'], 
                            'name_extension'    => $data['name_extension'], 
                            'email'             => $data['email'], 
                            'name'              => $data['name'], 
                            'password'          => bcrypt($data['password']),
                            'company_id'        => $company_id,
                            'role_id'           => 1,
                            'position_id'       => 0,
                            'department_id'     => 0,
                            'status'            => 1,
                            'registered_ip'     => $request->ip(),
                            'created_at'        => date('Y-m-d H:i:s')
                        );
            $res        = UserModel::saveCustomerData($user);

            $returnHTML  = view('customer_management.ajax.companies-table');
            echo $returnHTML;
        });       
    }    

    public function viewCustomerDetails(Request $request)
    {
        $data   = $request->all();
        $result = UserModel::getCustomerDetails($data);
        $returnHTML = view('customer_management.ajax.customer-details',
            [
                'customer_details'       => $result[0]
            ])->render();
        echo $returnHTML;
    }

    public function updateCustomerProfile(Request $request)
    {
        $data   = $request->all();
        $result = UserModel::updateCustomerProfile($data);
        $returnHTML  = view('customer_management.ajax.companies-table');
        echo $returnHTML;
    }

    public function companyProfile()
    {
        if (\Auth::user()->status == 1) {
            $data['company_details']        = UserModel::getCompanyDetails();
            $data['permissions']              = RoleModel::getAllPermissions();
            return view('company.index',$data);
        }else{
            return redirect('/inactive');
        }
    }
    public function updateCompanyProfile(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $data['function_name']  = $this->function;            
            $result                 = UserModel::updateCompanyProfile($data);
            //trail for update
            $data['function_name']  = $this->function;
            // $data['transaction_id'] = $data['id'];
            $updated_company_profile = UserModel::getCompanyDetails();
            // trailUpdate($data);
            return $updated_company_profile;
        });       
        return response()->json($result);
    }
    public function updateCompanyLogo(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);

        if ($validator->fails()) {
            Session::flash('error', 'Error!');
            return response()->json(0);
        }
        else{
        $data['company_id'] = Auth::user()->company_id;
        $image = $request->file('image');
        $data['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path().'/attachments/'.$data['company_id'].'/C-CP/';
        if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
        }
        $img = Image::make($image);
        $img->resize(200, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$data['imagename']);
        $result = UserModel::updateCompanyLogo($data);
        return response()->json($result);
    
        }
    }
    public function closingOfBooks(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $result                 = CustomerModel::assignedClosingOfBooks($data);
            // $result                 = UserModel::updateCompanyProfile($data);
            //trail for update
            // $data['function_name']  = $this->function;
            // $data['transaction_id'] = $data['id'];
            $updated_company_profile = UserModel::getCompanyDetails();
            // trailUpdate($data);
            return $updated_company_profile;
        });       
        return response()->json($result);

    }
    public function deleteCompanyLogo($id){
        $image = companyLogo();
        $company_id = Auth::user()->company_id;
        $destinationPath = public_path().'/attachments/'.$company_id.'/C-CP/';
        File::delete($destinationPath .'/'.$image);
        $result = UserModel::deleteCompanyLogo($id,$image);
        return back();
    }
    public function uiSettings()
    {
        if (\Auth::user()->status == 1) {
            $data['permissions']              = RoleModel::getAllPermissionsWithPDF();
            $ui_settings                      = CustomerModel::getUISettings();
            $data['items']              = $this->getUISettings($ui_settings);
            return view('ui-settings.index',$data);
        }else{
            return redirect('/inactive');
        }
    }
    private function getUISettings($ui_settings)
    {
        $data = [];
        foreach($ui_settings as $key => $value)
        {
            $items                      =  new \stdClass();
            $items->color               = $value->color;
            $items->function            = RoleModel::userRolesPermissionName($value->function_id);
            array_push($data, $items);
        }
        return $data;
    }
}   
