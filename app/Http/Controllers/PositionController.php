<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PositionModel;
use DB;

class PositionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->function = "Position";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('positions.index');
    }

    public function getAllPosition()
    {
        $result = PositionModel::getPosition();
        return response()->json($result);
    }
    public function getPosition()
    {
        $counter    = 1;
        $data       = PositionModel::getPosition();
        if ($data) {
            $counter    = 1;
            foreach ($data as $key => $value) {
                $result[]  = array(
                    '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                    'id'                    => $value->id,
                    'name'                  => '<p>'.$value->name.'</p>',
                    'action'                => '<button class="btn btn-primary btn-sm view-position-details" id="'.$value->id.'" title="View"><i class="tiny material-icons">visibility</i></button>',
                );
            }
        }else{
            $result[]  = array(
                '#'                     => 0,
                'id'                    => 'n/a',
                'name'                  => 'n/a',
                'status'                => 'n/a',
                'action'                => 'n/a',
            );
        }
        return response()->json($result);
    }
    public function addPosition(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $data['function_name']  = $this->function;
            $result                 = PositionModel::addPosition($data);
            $data['number']         = $result;
            //audit trail function
            trailCreate($data);
            return $result;
        });       
        return response()->json($result);
    }
    public function getPositionDetails(Request $request)
    {
        $data                       = $request->all();
        $data['position']         = PositionModel::getPositionDetails($data);
        return view('positions.ajax.position-details',$data);

    }
    public function updatePosition(Request $request)
    {
        $result = DB::transaction( function(&$data) use ($request) {
            $data                   = $request->all();
            //function name - always include this on creating
            $data['function_name']  = $this->function;
            $result                 = PositionModel::updatePosition($data);
            $data['number']         = $data['id'];
            //audit trail function
            trailUpdate($data);
            return $result;
        });       
        return response()->json($result);
    }
}
