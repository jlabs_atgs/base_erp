<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Auth;

class AuditTrailModel extends Model
{
	//create audit;
	public static function trailCreate($data)
	{
		return DB::table('audit_trail')
				->insert([
					'transaction_id' 	=> $data['number'],
					'function_name' 	=> $data['function_name'],
					'performed_by' 		=> Auth::user()->id,
					'date' 				=> date('Y-m-d H:i:s'),
					'ip_address' 		=> request()->ip(),
					'action' 			=> 'created',
					'company_id'		=> Auth::user()->company_id
				]);
	}

	//update audit
	public static function trailUpdate($data)
	{
		return DB::table('audit_trail')
				->insert([
					'transaction_id' 	=> $data['number'],
					'function_name' 	=> $data['function_name'],
					'performed_by' 		=> Auth::user()->id,
					'date' 				=> date('Y-m-d H:i:s'),
					'ip_address' 		=> request()->ip(),
					'action' 			=> 'updated',
					'company_id'		=> Auth::user()->company_id
				]);
	}

	//delete audit
	public static function trailDelete($data)
	{
		return DB::table('audit_trail')
				->insert([
					'transaction_id' 	=> $data['transaction_id'],
					'function_name' 	=> $data['function_name'],
					'performed_by' 		=> Auth::user()->id,
					'date' 				=> date('Y-m-d H:i:s'),
					'ip_address' 		=> request()->ip(),
					'action' 			=> 'deleted',
					'company_id'		=> Auth::user()->company_id
				]);
	}
	public static function getAuditTrailByUser($company_id)
	{
		return DB::table('audit_trail')
				->where('company_id',$company_id)
				->orderBy('id','desc')
				->get()->toArray();
	}
	//get single user details
    public static function getUserDetails($id)
    {
        return DB::table('users')
        		
                ->where('id', $id)
                ->first();
    }
    public static function getLastNumber($table,$data)
    {
    		return DB::table($table)
					->select('number')
					->orderBy('number','desc')
					->where('id',$data['id'])
					->where('company_id',Auth::user()->company_id)
					->first();
    }
}
