<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class DepartmentModel extends Model
{
	public static function saveDepartment($data)
    {
        $check = DB::table('departments')
                    ->where('name', $data)
                    ->get();
        if ($check->count() >= 1) {
            $id = DB::table('departments')
                    ->select('id')
                    ->where('name', $data)
                    ->get();
            return $id[0]->id;
        }else{
            DB::table('departments')
                ->insert([
                    'name'        => $data,
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
            return DB::getPdo()->lastInsertId();
        }
        
    }

    public static function getDepartment()
    {
        return DB::table('departments')
                ->where('company_id', Auth::user()->company_id)
                ->orWhere('company_id', 0)
                ->orderBy('id','desc')
                ->get()->toArray();
    }
    public static function getAllDepartmentExceptOwnDepartment($department_id)
    {
        return DB::table('departments')
                ->where('company_id', Auth::user()->company_id)
                ->where('id','!=',$department_id)
                ->orWhere('company_id', 0)
                ->orderBy('id','desc')
                ->get()->toArray();
    }
    public static function getDepartmentByUser()
    {
        return DB::table('pr_department_assigned')
                ->where('user_id',Auth::user()->id)
                ->where('company_id', Auth::user()->company_id)
                ->orderBy('id','desc')
                ->first();
    }
     public static function getOwnDepartmentByUser()
    {
        return DB::table('departments')
                ->where('id',Auth::user()->department_id)
                ->where('company_id', Auth::user()->company_id)
                ->orderBy('id','desc')
                ->get()->toArray();
    }
    public static function getDepartmentDetails($id)
    {
        return DB::table('departments')
                ->where('id',$id)
                ->first();
    }
    public static function assignedPRDepartment($data,$department_id)
    {
        $check = DB::table('pr_department_assigned')
                            ->where('user_id', $data['user_id'])
                            ->where('company_id',$data['company_id'])
                            ->get();
        if ($check->count() >= 1) {
            $id = DB::table('pr_department_assigned')
                    ->where('user_id', $data['user_id'])
                    ->where('company_id',$data['company_id'])
                    ->update([
                        'dept_id'       => $department_id,
                        'updated_at'    => date('Y-m-d H:i:s')
                    ]);
            // return $id[0]->id;
        }else{
            DB::table('pr_department_assigned')
                ->insert([
                    'user_id'        => $data['user_id'],
                    'dept_id'        => $department_id,
                    'company_id'     => Auth::user()->company_id,
                    'created_at'     => date('Y-m-d H:i:s'),
                    'created_by'     => Auth::user()->id
                ]);
            return DB::getPdo()->lastInsertId();
        }
    }
    public static function pr_department_assigned($user_id)
    {
        return DB::table('pr_department_assigned')
                    ->where('user_id',$user_id)
                    ->get()->toArray();
    }
    public static function nonAssignedDepart($user_id,$company_id)
    {
        return DB::table('pr_department_assigned')
                ->where('user_id', $user_id)
                ->where('company_id',$company_id)
                ->update([
                    'dept_id'   => ''
                ]);
    }
    public static function addDepartment($data)
    {
        $insert =  DB::table('departments')
                ->insert([
                    'name'        => $data['name'],
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
        return DB::getPdo()->lastInsertId();
    }
    public static function updateDepartment($data)
    {
        return DB::table('departments')
                ->where('id',$data['id'])
                ->update([
                    'name'        => $data['name'],
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
    }
    public static function getDepartmentID($data)
    {
        return DB::table('departments')
                ->where('name', $data['department_name'])
                ->where('company_id', Auth::user()->company_id)
                ->first();
    }
    public static function getDepartmentLimit($term,$skip,$count)
    {
        if ($term) {
                return DB::table('departments')
                    ->select(DB::raw('
                        id,
                        name AS text
                    '))
                    ->where('name', 'LIKE', '%' . $term. '%')
                    ->where('company_id', Auth::user()->company_id)
                    ->skip($skip)
                    ->take($count)
                    ->get()->toArray(); 
            
        }else{
            return DB::table('departments')
                    ->select(DB::raw('
                        id,
                        name AS text
                    '))
                    ->where('name', 'LIKE', '%' . $term. '%')
                    ->where('company_id', Auth::user()->company_id)
                    ->skip($skip)
                    ->take($count)
                    ->get()->toArray(); 
        }
    }
    public static function countDepartment()
    {
        return DB::table('departments')
                // ->where('status', '1')
                ->where('company_id', Auth::user()->company_id)
                ->orderBy('code','ASC')
                ->count();
    }
}
