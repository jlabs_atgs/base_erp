<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class RoleModel extends Model
{
    public static function getAllRoles()
    {
    	return DB::table('user_roles')
    				->where('company_id', Auth::user()->company_id)
    				->orWhere('status', 99)
    				->get();
    }

    public static function getRoleDetails($data)
    {
    	$check	= DB::table('user_roles')
    				->where('id', $data['id'])
    				->get();
    	$array = explode(',', $check[0]->permission);
    	$result = DB::table('user_roles_permission')
    				->select('id', 'permission_name')
    				->whereIn('id', $array)
    				->get();
    	return array($check, $result);
    }

    public static function updateRolePermissions($data, $permission)
    {
    	$array 	= array($data, $permission);
    	return DB::transaction( function(&$data) use ($array) {
    		return DB::table('user_roles')
    					->where('id', $array[0]['role_id'])
    					->update([
                            'name'       => $array[0]['name'],
    						'permission' => $array[1],
    						'updated_at' => date('Y-m-d H:i:s'),
    						'status' 	 => $status = ( $array[0]['status'] == 'ACTIVE' ? 1 : 0 )
    					]);
        });   
    }

    public static function addRoleInformation($data, $permission)
    {
    	$array 	= array($data, $permission);
    	return DB::transaction( function(&$data) use ($array) {
    		return DB::table('user_roles')
    					->where('company_id', Auth::user()->id)
    					->insert([
    						'name' 		 => $array[0]['role_name'],
    						'permission' => $array[1],
    						'created_at' => date('Y-m-d H:i:s'),
    						'status' 	 => 1,
    						'company_id' => Auth::user()->company_id,
    						'created_by' => Auth::user()->id
    					]);
        });   
    }

    public static function getPermissions( $data )
    {
        $result = DB::table('users as u')
            ->select('permission')
            ->join('user_roles as r', 'r.id', '=', 'u.role_id')
            ->where('u.id', '=', $data['user_id'] )
            ->get();

        return $result;
    }

    public static function userRolesPermission($number)
    {
        return DB::table('user_roles_permission')
                ->select('id')
                ->where('id', $number)
                ->get();
    }

    public static function userRolesPermissionName($number)
    {
        return DB::table('user_roles_permission')
                ->select('permission_name')
                ->where('id', $number)
                ->first();
    }

    public static function userPermissions($user_id, $role_id)
    {
        return DB::table('users as u')
                ->select('r.permission')
                ->leftJoin('user_roles as r', 'u.role_id', '=', 'r.id')
                ->where('r.id', $role_id)
                ->where('u.id', $user_id)
                ->get();
    }

    public static function getAllPermissions()
    {
        return DB::table('user_roles_permission')
                ->orderBy('id', 'asc')
                ->get();
    }

    public static function getRoleName($data)
    {
        return DB::table('user_roles')
                ->where('id', $data['id'])
                ->get();
    }

    public static function getUserRoleName($id)
    {
        return DB::table('user_roles')
                ->where('id', $id)
                ->get();
    }

}
